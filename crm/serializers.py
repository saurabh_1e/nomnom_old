from rest_framework import serializers
from crm.models import Customer, ContactNumber, Address, City, Locality, Order, OrderLineItem,\
    DeliveryBoy, Agent, DeliveryBoyLocation, OrderLog, Ticket

from restaurant.serializers import RestaurantOrderSerializer, DeliveryDishVariationSerializer
from restaurant.models import Restaurant, DishVariation
import datetime
from utilities.google_maps import GoogleMaps
from nomnom.choices import  *


class AgentInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Agent
        fields = ('id', 'name', 'contact_number', 'agent_type', 'cogent_agent_id')
        depth = 1


class AgentDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Agent
        fields = ('id', 'name', 'username', 'contact_number', 'permanent_address', 'current_address', 'agent_type',
                  'is_logged_in')


class ContactNumberSerializer(serializers.ModelSerializer):


    class Meta:
        model = ContactNumber

class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City


class LocalitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Locality
        depth = 1


class AddressViewSerializer(serializers.ModelSerializer):
    locality = LocalitySerializer()
    city_name = serializers.SerializerMethodField(source='get_city_name')

    class Meta:
        model = Address
        fields = ('id', 'complete_address', 'city', 'city_name', 'locality', 'customer', 'address_type',
                  'latitude', 'longitude')

    def get_city_name(self, obj):
        return City.objects.get(pk=obj.city_id).name


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        model = Address


class CustomerSerializer(serializers.ModelSerializer):
    alternate_numbers = serializers.SerializerMethodField(source='get_alternate_numbers')

    class Meta:
        model = Customer
        fields = ('id', 'name', 'primary_number', 'alternate_numbers')

    def get_alternate_numbers(self, obj):
        numbers = ContactNumber.objects.filter(customer_id=obj.id)
        return [ {"number": num.number, "type": num.number_type}  for num in numbers]


class CustomerDetailSerializer(serializers.ModelSerializer):

    alternate_numbers = ContactNumberSerializer(many=True)
    address_list = AddressViewSerializer(many=True)

    class Meta:
        model = Customer
        fields = ('id', 'name', 'primary_number', 'alternate_numbers', 'address_list', 'call_count')


class DeliveryBoyDetailSerializer(serializers.ModelSerializer):

    agent = AgentInfoSerializer
    order_status = serializers.SerializerMethodField(source='get_order_status')

    class Meta:
        model = DeliveryBoy
        depth = 1
        fields = ('id', 'agent', 'bike_number', 'latitude', 'longitude', 'latlon_timestamp', 'is_available',
                  'order_status', 'current_order')

    def update(self, instance, validated_data):
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.longitude = validated_data.get('longitude', instance.longitude)
        instance.latlon_timestamp = datetime.datetime.now()
        instance.save()
        return True

    def get_order_status(self, obj):
        return obj.current_order.status if obj.current_order else None


class DeliveryBoyLocationSerializer(serializers.ModelSerializer):

    delivery_boy = serializers.IntegerField()

    class Meta:
        model = DeliveryBoyLocation

    def create(self, validated_data):

        boy = validated_data.pop('delivery_boy')
        delivery_boy_serializer = DeliveryBoyDetailSerializer()
        delivery_boy_serializer.update(instance=boy, validated_data=validated_data)

        DeliveryBoyLocation.objects.create(delivery_boy=boy, **validated_data)

        return True


class OrderLineItemOrderCreateSerializer(serializers.ModelSerializer):
    # create a new line item

    dish_variation = serializers.IntegerField()

    class Meta:
        model = OrderLineItem
        exclude = ('id', 'order')

    def create(self, validated_data):
        return OrderLineItem(**validated_data)


class OrderCreateSerializer(serializers.ModelSerializer):

    # create a new order

    customer = serializers.IntegerField()
    restaurant = serializers.IntegerField()
    order_items = OrderLineItemOrderCreateSerializer(many=True)
    address = serializers.IntegerField()

    class Meta:
        model = Order
        depth = 1
        fields = ('order_items', 'customer', 'restaurant', 'sub_total', 'other_charges',
                  'total_amount', 'vat_tax', 'service_tax', 'discount_amount',
                  'special_instructions', 'address', 'status', 'payment_cash', 'payment_card',
                  'order_source')

    def create(self, validated_data):

        # Get attributes for order record
        agent = validated_data.pop("agent")
        customer_id = validated_data.pop('customer')
        restaurant = validated_data.pop('restaurant')
        address = validated_data.pop('address')
        items_inline_list = validated_data.pop('order_items')
        print items_inline_list
        # Get foreign key objects for an order
        customer = Customer.objects.get(id=customer_id)
        restaurant = Restaurant.objects.get(id=restaurant)
        address = Address.objects.get(id=address)

        # calculates estimated delivery time by summing up restaurant delivery time,
        #  travelling time by google maps, current time
        # try:
        #     seconds = GoogleMaps().estimate_delivery_time(address, (restaurant.latitude, restaurant.longitude))
        # except Exception as e:
        seconds = 15*60

        expected_delivery_time = datetime.timedelta(minutes=restaurant.estimated_preparation_time, seconds=seconds) + \
            datetime.datetime.now()

        expected_pickup_time = datetime.timedelta(minutes=restaurant.estimated_preparation_time) + \
            datetime.datetime.now()

        # Creating objects of order items in OrderLineItem table

        if len(items_inline_list):
            order = Order.objects.create(agent=agent, customer=customer, restaurant=restaurant, address=address,
                                         expected_delivery_time=expected_delivery_time,
                                         expected_pickup_time = expected_pickup_time, status= DELIVERY_STATUS[0][0],
                                         **validated_data)
            order.save()
            for item in items_inline_list:
                dish_variation = DishVariation.objects.get(id=item.pop('dish_variation'))
                OrderLineItem.objects.create(order=order, dish_variation=dish_variation, **item)

            return order
        else:
            return False

    def update(self, instance, validated_data):

        order_items = validated_data.pop('order_items')
        #line_item_total = 0
        for item in order_items:

            order_line_item = OrderLineItemSerializer()
            # Delete the item if quantity is zero else update the quantity or add a new item
            if 'id' in item:
                line_item_instance = OrderLineItem.objects.get(pk=int(item['id']))
                if int(item['quantity']) == 0:
                    line_item_instance.delete()
                    #line_item_total -= int(item["total_price_charged"])
                else:
                    order_line_item.update(instance=line_item_instance, validated_data=item)
                    #line_item_total += int(item["total_price_charged"])
            else:
                dish_variation = DishVariation.objects.get(pk = item["dish_variation"])
                item.pop("dish_variation")
                OrderLineItem.objects.create(order=instance, dish_variation=dish_variation, **item)
                #line_item_total += int(item["total_price_charged"])

        instance.address_id = validated_data.pop("address")
        instance.sub_total = validated_data.pop("sub_total")
        instance.vat_tax = validated_data.pop("vat_tax")
        instance.service_tax = validated_data.pop("service_tax")
        instance.other_charges = validated_data.pop("other_charges")
        instance.discount_amount = validated_data.pop("discount_amount")
        instance.total_amount = validated_data.pop("total_amount")

        instance.save()
        return instance


class OrderLogSerializer(serializers.ModelSerializer):
    owner_name = serializers.SerializerMethodField(source='get_owner_name')


    class Meta:
        model = OrderLog
        fields = ('id', 'owner_id', 'order', 'owner_name', 'owner_type', 'message', 'created_on')


    def get_owner_name(self, obj):
        if obj.owner_type in [AGENT_TYPE[0][0], AGENT_TYPE[2][0], AGENT_TYPE[3][0]]:
            return Agent.objects.get(pk=obj.owner_id).name
        else:
            return "System"


class OrderLineItemSerializer(serializers.ModelSerializer):

    dish_variation = DeliveryDishVariationSerializer()

    class Meta:
        model = OrderLineItem

    def update(self, instance, validated_data):
        instance.quantity = validated_data.get('quantity', instance.quantity)
        instance.unit_price_charged = validated_data.get('unit_price_charged', instance.unit_price_charged)
        instance.total_price_charged = validated_data.get('total_price_charged', instance.total_price_charged)
        instance.save()
        return True

    def create(self, validated_data):
        OrderLineItem.objects.create(**validated_data)
        return True


class OrderViewLineItemSerializer(serializers.ModelSerializer):

    # Used when sending info on delivery of order or list of orders

    dish_variation = DeliveryDishVariationSerializer()

    class Meta:
        model = OrderLineItem
        exclude = ('order', )

class OrderViewSerializer(serializers.ModelSerializer):

    # Used for delivery order or list of orders json

    customer = CustomerSerializer()
    address = AddressViewSerializer()
    restaurant = RestaurantOrderSerializer()
    order_items = OrderViewLineItemSerializer(many=True)
    delivery_boy = DeliveryBoyDetailSerializer()

    class Meta:
        model = Order
        fields = ('id', 'customer', 'address', 'restaurant', 'order_items', 'delivery_boy',
                  'vat_tax', 'service_tax', 'discount_amount', 'other_charges',
                  'total_amount', 'special_instructions', 'delivery_instructions', 'restaurant_amount',
                  'status', 'expected_delivery_time', 'expected_pickup_time',
                  'payment_cash', 'payment_card', 'created_on', 'updated_on')


class OrderDetailViewSerializer(serializers.ModelSerializer):

    customer = CustomerSerializer()
    restaurant = RestaurantOrderSerializer()
    order_items = OrderLineItemSerializer(many=True)
    address = AddressViewSerializer()
    order_logs = OrderLogSerializer(many=True)
    agent = AgentInfoSerializer()
    delivery_boy = DeliveryBoyDetailSerializer()

    class Meta:
        model = Order
        depth = 1
        include = ('order_items', 'customer', 'restaurant', 'sub_total', 'other_charges',
                   'total_amount', 'vat_tax', 'service_tax', 'discount_amount',
                   'special_instructions', 'delivery_instructions', 'address', 'status', 'payment_card', 'payment_cash', 'order_logs',
                   'created_on', 'updated_on')


class TicketSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket


class TicketViewSerializer(serializers.ModelSerializer):
    created_by = AgentInfoSerializer()
    assigned_to = AgentInfoSerializer()

    class Meta:
        model = Ticket