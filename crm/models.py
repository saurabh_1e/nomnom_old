import binascii
import os
import math
import time
from datetime import datetime

from django.db import models
from django.contrib.auth.models import UserManager
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import AbstractBaseUser
from django.core.exceptions import ObjectDoesNotExist
from nomnom.choices import *
from utilities.google_gcm import GCM
from utilities.Logger import Logger
from utilities.sms import SMS
from django.db.models import Q
from utilities.raygun import beam


class TimeStampedModel(models.Model):
    """ TimeStampedModel
    An abstract base class model that provides self-managed "created" and
    "modified" fields.
    """
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    class Meta:
        get_latest_by = 'updated_on'
        ordering = ('-updated_on', '-created_on',)
        abstract = True


class Agent(TimeStampedModel, AbstractBaseUser):

    # Phone picking or order delivery agent or operations executive

    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    contact_number = models.CharField(max_length=100, unique=True)
    permanent_address = models.CharField(max_length=255)
    cogent_agent_id = models.CharField(max_length=255, null=True)
    current_address = models.CharField(max_length=255)
    agent_type = models.CharField(max_length=25, choices=AGENT_TYPE, default=AGENT_TYPE[0][0])
    is_logged_in = models.BooleanField(default= False)

    objects = UserManager()

    USERNAME_FIELD = 'username'

    class Meta:
        ordering = ('created_on',)

    def __str__(self):
        return "%s - %s" % (self.name, self.contact_number)

    def update_delivery_boy(self, status):
        DeliveryBoy.objects.filter(agent=self).update(is_available=status)

    def get_auth_token(self, headers):
        (device_id, device_type, push_id) = headers["HTTP_X_DEVICE_ID"],\
                                            headers["HTTP_X_DEVICE_TYPE"],\
                                            headers["HTTP_X_PUSH_ID"]

        app_version_code = headers['HTTP_X_VERSION_CODE'] if 'HTTP_X_VERSION_CODE' in headers else None
        app_name = headers['HTTP_X_APP_NAME'] if 'HTTP_X_APP_NAME' in headers else None

        CRMAccessToken.objects.filter(device_id= device_id, device_type=device_type).update(is_active=False)

        token = CRMAccessToken.objects.create(user=self, device_id=device_id, device_type=device_type,
                                              push_id=push_id, app_version_code=app_version_code, app_name=app_name)

        return token

    def login(self, headers):
        self.is_logged_in = True
        token = self.get_auth_token(headers)
        self.update_delivery_boy(status=True)
        self.save()
        AgentLog.objects.create(agent=self, action="login")
        return token.access_token


@python_2_unicode_compatible
class CRMAccessToken(models.Model):

    access_token = models.CharField(max_length=50, primary_key=True)
    user = models.ForeignKey(Agent, related_name='auth_token')
    device_id = models.CharField(max_length=255, default="web")
    device_type = models.CharField(max_length=10, null=False)
    push_id = models.CharField(max_length=255, default=None)
    is_active = models.BooleanField(default=True)
    app_version_code = models.CharField(max_length=10, null=True)
    app_name = models.CharField(max_length=20, null=True)
    created_on = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        if not self.access_token:
            self.access_token = self.generate_token()
        return super(CRMAccessToken, self).save(*args, **kwargs)

    def generate_token(self):
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.access_token


class CallLog(TimeStampedModel):

    agent = models.ForeignKey(Agent)
    customer = models.ForeignKey('Customer')
    forwarded_for = models.CharField(max_length=30, null=True)

    @staticmethod
    def log_call(customer, agent_id, forwarded_for="8882288822"):

        try:
            agent = Agent.objects.filter(cogent_agent_id= agent_id)[0]
            CallLog.objects.create(agent=agent, customer=customer, forwarded_for=forwarded_for)
        except Exception as e:
            print e

        return True


class City(models.Model):
    name = models.CharField(max_length=120, null=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)


class Locality(models.Model):

    name = models.CharField(max_length=120, null=False)
    city = models.ForeignKey(City)
    latitude = models.DecimalField(decimal_places=6, max_digits=10, default=0.0)
    longitude = models.DecimalField(decimal_places=6, max_digits=10, default=0.0)
    delivery_radius = models.IntegerField(default=0, help_text='In meters')

    def __str__(self):
        return "%s - %s" % (self.id, self.name)


class Customer(TimeStampedModel, AbstractBaseUser):

    name = models.CharField(max_length=255, default='New Customer')
    primary_number = models.CharField(max_length=255, unique=True)
    age = models.IntegerField(default=12)
    customer_category = models.CharField(max_length=20, choices=CUSTOMER_CATEGORY, null=True)
    max_spend = models.IntegerField(default=0)
    median_spend = models.IntegerField(default=0)
    lowest_spend = models.IntegerField(default=0)
    payment_preferences = models.CharField(max_length= 50, choices=PAYMENT_PREFERENCES, null=True)
    wants_beverage = models.BooleanField(default= False)
    wants_dessert = models.BooleanField(default= False)
    allergies = models.CharField(max_length= 255, null=True)
    gender = models.CharField(max_length=10, choices=GENDER, default='F')
    language_preference = models.CharField(max_length=12, choices=LANGUAGES, default='HINDI')
    rating_for_nomnom = models.IntegerField(null=True)
    call_count = models.IntegerField(default=1)

    objects = UserManager()

    USERNAME_FIELD = 'primary_number'

    @staticmethod
    def find_by_contact_number(contact_number):

        try:
            customer = Customer.objects.get(primary_number= contact_number)
        except ObjectDoesNotExist:
            try:
                customer = ContactNumber.objects.filter(number= contact_number)[0].customer
            except IndexError:
                customer = False

        return customer

    def get_pending_order(self):
        #Check for pending deliveries from this order
        try:
            order = Order.objects.filter(customer=self).last()
            if order.status == 'delivered' or order.status == 'cancelled':
                return None
            else:
                return order
        except Exception as e:
            pass


    class Meta:
        ordering = ('updated_on',)

    def __str__(self):
        return "%s - %s" % (self.name, self.primary_number)


class CallBackRequest(TimeStampedModel):

    contact_number = models.CharField(max_length=255, null=False)
    source = models.CharField(max_length=55, default="website")
    requested_by = models.CharField(max_length=255, null=True)


class Address(TimeStampedModel):

    customer = models.ForeignKey(Customer, related_name='address_list')
    complete_address = models.CharField(max_length=500, null=True)
    city = models.ForeignKey(City)
    locality = models.ForeignKey(Locality)
    address_type = models.CharField(max_length=20, choices=ADDRESS_TYPE, default=ADDRESS_TYPE[0][0])
    latitude = models.DecimalField(decimal_places=6, max_digits=8, default=0.0)
    longitude = models.DecimalField(decimal_places=6, max_digits=8, default=0.0)

    class Meta:
        ordering = ('updated_on',)


class ContactNumber(TimeStampedModel):

    customer = models.ForeignKey(Customer, related_name='alternate_numbers')
    number = models.CharField(max_length=255)
    number_type = models.CharField(max_length=10, choices=ADDRESS_TYPE)

    class Meta:
        ordering = ('updated_on', )


class CustomerPreference(models.Model):
    # TODO
    customer = models.ForeignKey(Customer)
    # favorite_dish_one = models.ForeignKey(, null=True)
    # favorite_dish_two = models.ForeignKey(, null=True)
    # favorite_dish_three = models.ForeignKey( , null=True)
    # favorite_cuisine_one = models.ForeignKey( , null=True)
    # favorite_cuisine_two = models.ForeignKey( , null=True)
    # favorite_cuisine_three = models.ForeignKey( , null=True)
    # favorite_place_one = models.ForeignKey( , null=True)
    # favorite_place_two = models.ForeignKey( , null=True)
    # favorite_place_three= = models.ForeignKey( , null=True)


class DeliveryBoy(models.Model):

    agent = models.ForeignKey(Agent,related_name="delivery_agent")
    bike_number = models.CharField(max_length=20)
    dl_number = models.CharField(max_length=25)
    rc_number = models.CharField(max_length=25)
    is_available = models.BooleanField(default=True)
    latitude = models.DecimalField(decimal_places=6, max_digits=8)
    longitude = models.DecimalField(decimal_places=6, max_digits=8)
    latlon_timestamp = models.DateTimeField()
    current_order = models.ForeignKey('Order', null=True)

    @staticmethod
    def get_available_delivery_boy(order, radius=100):
        latitude = float(order.restaurant.latitude)
        longitude = float(order.restaurant.longitude)
        radius = float(radius)
        # Get delivery boys within a 10 KM radius
        delivery_boys = DeliveryBoy.objects.raw("SELECT *,"
                                                " SQRT( POW(69.1 * (latitude - %f), 2) +"
                                                " POW(69.1 * (longitude - %f) * "
                                                "COS(latitude / 57.3), 2)) AS distance FROM "
                                                "crm_deliveryboy HAVING is_available = 1 and"
                                                " distance < %i  ORDER BY distance;" % (latitude, longitude, radius))

        try:
            return delivery_boys[0]
        except Exception as e:
            return None

    def get_push_id(self):
        try:
            token = CRMAccessToken.objects.filter(user=self.agent,
                                               is_active=True)[0]
            return token.push_id
        except IndexError:
            return False

    def notify(self, payload):
        push_id = self.get_push_id()
        if push_id:
            gcm = GCM()
            gcm.send_message([push_id], payload)
        try:
            s = SMS()
            s.send(mobile_number=self.agent.contact_number, sms_text= "Order Id: %s" % payload["order_id"])
            print 'Sms sent'
        except Exception as e:
            beam(e)


    def __str__(self):
        return "%s - %s" % (self.agent.name, self.agent.contact_number)


class DeliveryBoyLocation(models.Model):
    """
    Log delivery boy longitude and latitude
    """

    delivery_boy = models.ForeignKey('DeliveryBoy')
    latitude = models.DecimalField(decimal_places=6, max_digits=8)
    longitude = models.DecimalField(decimal_places=6, max_digits=8)
    latlon_timestamp = models.DateTimeField(auto_now=True)


class AgentLog(models.Model):
    """
    Log delivery boy's login and logout data
    """
    agent = models.ForeignKey('Agent')
    action = models.CharField(max_length=25, choices=(("login", "Login"), ("logout", "Logout")))
    created_on = models.DateTimeField(auto_now=True)


class Order(TimeStampedModel):

    agent = models.ForeignKey(Agent, related_name="order_taking_agent")
    customer = models.ForeignKey(Customer, related_name='order_customer')
    address = models.ForeignKey('Address', related_name= 'customer_order_address')
    restaurant = models.ForeignKey('restaurant.Restaurant', related_name='order_restaurant')
    delivery_boy = models.ForeignKey('DeliveryBoy', related_name='assigned_delivery_boy', null=True)

    sub_total = models.DecimalField(max_digits=8, decimal_places=3)
    vat_tax = models.DecimalField(max_digits=6, decimal_places=2)
    service_tax = models.DecimalField(max_digits=6, decimal_places=2)
    discount_amount = models.IntegerField(default=0, null=True)
    other_charges = models.IntegerField(default=0)
    total_amount = models.DecimalField(max_digits=8, decimal_places=3)
    bill_image = models.ImageField(upload_to='static/bills', blank=True)
    special_instructions = models.CharField(max_length=1000)

    status = models.CharField(max_length=40, choices=DELIVERY_STATUS)

    order_communicated_through = models.CharField(max_length=255, null=True)
    restaurant_notified = models.BooleanField(default=False)
    restaurant_amount = models.DecimalField(max_digits=8, decimal_places=3, null=True,
                                            help_text="Amount to be paid to the restaurant")


    expected_delivery_time = models.DateTimeField(null=True, blank=True)
    expected_pickup_time = models.DateTimeField(null=True, blank=True)

    delivered_at = models.DateTimeField(null=True, blank=True)
    delivery_instructions = models.CharField(max_length=225, null=True, blank=True)
    delayed_by = models.IntegerField(default=0)

    payment_cash = models.IntegerField(blank=True, null=True)
    payment_card = models.IntegerField(blank=True, null=True)
    discount_on_delivery = models.IntegerField(blank=True, null=True)

    order_source = models.CharField(max_length=255, null=True, blank=True)

    class Meta:
        ordering = ('updated_on', )

    def should_call(self):
        ticket_id, numbers = None, None

        if self.restaurant.has_to_be_called:
            numbers = [c.number for c in self.restaurant.numbers.all() if c.number_type == NUMBER_TYPE[2][0]]
            ticket_id = Logger.log_ticket(order=self, message="System ticket created to place order via "
                                                  "outbound call with restaurant", owner=self.agent,
                              owner_type="Agent", ticket_type='place_order')
        #else:
        #self.notify_restaurant()

        return {
                "order": self,
                "estimated_delivery_time": 20, #abs((self.expected_delivery_time - datetime.now()).minutes),
                "contact_numbers": numbers,
                "ticket_id": ticket_id
                }

    def billing(self, line_item_total=0, **kwargs):
        self.vat_tax = float(kwargs.pop("vat_tax", 0))
        self.service_tax = float(kwargs.pop("service_tax", 0))
        self.discount_amount = int(kwargs.pop("discount_amount", 0))
        self.other_charges = int(kwargs.pop("other_charges", 0))
        self.total_amount = line_item_total + self.taxes + self.other_charges - self.discount_amount

    def notify_restaurant(self):
        if self.restaurant.has_gps_printer:
            # Send GPS push
            pass
            return True
        elif self.restaurant.has_merchant_app:
            # Send GCM Push
            pass
            return True
        else:
            try:
                for contact in self.restaurant.numbers.all():
                    if contact.number_type == NUMBER_TYPE[1][0]:
                        s = SMS()
                        s.send(mobile_number=contact.number, sms_text=self.order_text())
                        OrderLog.objects.create(order=self, message="Restaurant notified via SMS, to"
                                                                    " be followed up by a call",
                                                owner_type='system', owner_id=self.agent.id)
                        return True
            except ObjectDoesNotExist:
                Logger.log_ticket(order=self, message=dict(STOCK_MESSAGES)['failed_to_notify'],
                                  owner=self.agent, owner_type='system', ticket_type=TICKET_TYPE[1][0])
                return False

    def delivery_boy_assigned(self):

        counter = 0
        while counter < 7:
            if self.delivery_boy is not None and self.status == 'assigned':
                return True
            counter += 1
            time.sleep(10)
            self.refresh_from_db()

        else:
            return False

    def get_cart(self):
        cart = {
            "categories": [],
            "sub_total": str(self.sub_total),
            "vat_tax": str(self.vat_tax),
            "service_tax": str(self.service_tax),
            "other_charges": self.other_charges,
            "discount_amount": self.discount_amount,
            "total_amount": str(self.total_amount),
            "special_instructions": self.special_instructions,
            "delivery_instructions": self.delivery_instructions
        }
        cart_categories = dict(CART_CATEGORY)
        items = OrderLineItem.objects.filter(order=self)
        i = 0
        cat_index = {}
        for item in items:

            #See if particular dish cart category has already been
            #appened in the categories array or not
            if item.dish_variation.dish.cart_category in cat_index:
                index = cat_index[item.dish_variation.dish.cart_category]
            else:
                cat_index[item.dish_variation.dish.cart_category] = i
                cart["categories"].append({
                    "name": cart_categories[item.dish_variation.dish.cart_category],
                    "slug": item.dish_variation.dish.cart_category,
                    "items": []
                })
                index = i
                i += 1

            cart["categories"][index]["items"].append({
                    "id": item.id,
                    "dish_name": item.dish_variation.dish.dish_name,
                    "portion_size": item.dish_variation.portion,
                    "variant": item.dish_variation.variety,
                    "dish_variation": item.dish_variation.id,
                    "quantity": item.quantity,
                    "unit_price_charged": item.unit_price_charged,
                    "total_price_charged": item.total_price_charged
                    })

        return cart

    def __str__(self):
        return str(self.id)

    @property
    def taxes(self):
        return self.vat_tax + self.service_tax

    def order_line_item(self, item):
        dish_variation = item.dish_variation
        if dish_variation.variety and dish_variation.variety != 'na':
            return "%s %s %s" % (dish_variation.portion, dish_variation.variety, dish_variation.dish.dish_name)
        else:
            return "%s %s" % (dish_variation.portion, dish_variation.dish.dish_name)

    def order_text(self):

        opening_text = "NomNom Order ID - %s \n\n" % (self.id, )

        order_text = "Order Details: \n"
        items = OrderLineItem.objects.filter(order=self)
        for item in items:
            line_string = ("%sX %s= Rs.%s\n" % (item.quantity, self.order_line_item(item), item.total_price_charged))
            order_text += line_string

        if self.special_instructions is not None and self.special_instructions.__len__() > 0:
            special_instructions = "\nOrder Instructions: %s\n" % (self.special_instructions, )
        else:
            special_instructions = ''

        closing_text = "\nPlaced at: %s \nPick Up time: %s\n\n" % \
                       (self.created_on.strftime("%I:%M"), self.expected_pickup_time.strftime("%I:%M"))

        amount_text = "\nTotal Amount: %s\nAmount to be collected from rider: %s\n" % \
                                     (str(self.total_amount), str(self.total_amount))

        customer_info = "Customer name: %s \nCustomer Phone: %s\nCustomer Address: %s\n\n" % \
                       (self.customer.name, self.customer.primary_number, str(self.address.complete_address + ' ' + self.address.locality.name +
                                                                              ' ' + self.address.city.name))

        final_text = opening_text + order_text + special_instructions + amount_text + closing_text + customer_info

        return final_text

    def deassign_delivery_boy(self):
         if self.delivery_boy:
            self.delivery_boy.is_available = True
            self.delivery_boy.current_order_id = None
            self.delivery_boy.save()

    def assign_delivery_boy(self, delivery_boy):
        delivery_boy.is_available = False
        delivery_boy.current_order_id = self.id
        delivery_boy.save()
        self.deassign_delivery_boy()
        self.delivery_boy = delivery_boy
        self.save()


class OrderLineItem(models.Model):

    # dish variation ordered
    dish_variation = models.ForeignKey('restaurant.DishVariation', default=1)
    quantity = models.IntegerField()
    unit_price_charged = models.IntegerField()
    total_price_charged = models.IntegerField()
    order = models.ForeignKey(Order, null=True, related_name='order_items')

    def __str__(self):
        return "order %s - %s" % (self.order.id, self.dish_variation.dish.dish_name)


class OrderLog(models.Model):

    order = models.ForeignKey('Order', related_name='order_logs')
    message = models.CharField(max_length=255)
    owner_id = models.IntegerField()
    owner_type = models.CharField(max_length=25)
    created_on = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('created_on', )


class Alarm(TimeStampedModel):

    order = models.ForeignKey(Order)
    for_status = models.CharField(max_length=255)
    delayed_by = models.IntegerField(default=0)
    is_active = models.BooleanField(default=True)


class Ticket(TimeStampedModel):

    created_by = models.ForeignKey(Agent, related_name="ticket_by")
    assigned_to = models.ForeignKey(Agent, related_name="ticket_for", null=True)
    ticket_type = models.CharField(max_length=55, choices=TICKET_TYPE)
    is_active = models.BooleanField(default=True)
    order = models.ForeignKey('Order')
    details = models.CharField(max_length=1024)
    resolved_at = models.DateTimeField(null=True)

    class Meta:
        ordering = ('created_on',)
