from django.conf.urls import url
from views import AgentView, AgentLogInView, AgentLogOutView, CallView, CustomerView, ContactNumberView, \
    AddressView, OrderView, OrderLogView, DeliveryView, DeliveryBoyView, TicketView, \
    LocalityView, BillImage, CallBackRequestView, ContactUsView, upload_file

urlpatterns = [
    url(r'agent/', AgentView.as_view()),
    url(r'agent_login/', AgentLogInView.as_view()),
    url(r'agent_logout/', AgentLogOutView.as_view()),

    url(r'call/', CallView.as_view()),

    url(r'customer/', CustomerView.as_view()),
    url(r'contact_number/', ContactNumberView.as_view()),
    url(r'address/', AddressView.as_view()),
    url(r'locality/', LocalityView.as_view()),

    url(r'order/', OrderView.as_view()),
    url(r'order_log/', OrderLogView.as_view()),

    url(r'delivery/', DeliveryView.as_view()),
    url(r'delivery_boy/', DeliveryBoyView.as_view()),
    url(r'bill_image', BillImage.as_view()),

    url(r'ticket/', TicketView.as_view()),

    #Website related endpoints
    url(r'callback_request/', CallBackRequestView.as_view()),

    url(r'contact_us/', ContactUsView.as_view()),

    url(r'upload_file/', upload_file)


]