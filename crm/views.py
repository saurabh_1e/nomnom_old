import datetime
import sys
import traceback
import json
import django.utils.timezone

from restaurant.models import PhoneNumber
from nomnom.APIPermissions import AuthToken
from nomnom.choices import *
from utilities.google_maps import GoogleMaps
from utilities.cosmo_call import cosmocall_request
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, JSONParser
from django.core.exceptions import ObjectDoesNotExist
from nomnom.response import JSONResponse
from utilities.push_message import PushMessage
from utilities.raygun import beam

from serializers import CustomerSerializer, ContactNumberSerializer, \
    CitySerializer, LocalitySerializer, CustomerDetailSerializer, OrderCreateSerializer, OrderViewSerializer, \
    OrderDetailViewSerializer, DeliveryBoyLocationSerializer, OrderLogSerializer, TicketSerializer, \
    TicketViewSerializer, DeliveryBoyDetailSerializer, AgentDetailSerializer, AgentInfoSerializer, AddressSerializer,\
    AddressViewSerializer, OrderLineItemOrderCreateSerializer

from restaurant.serializers import ClientSerializer

from models import City, Locality, Customer, Order, DeliveryBoy, OrderLog, Agent, Ticket, AgentLog,\
    CRMAccessToken, Address, CallLog, CallBackRequest


class AgentView(APIView):
    # Endpoint: agent/

    permission_classes = (AuthToken, )

    def get(self, request):
        """


        summary: Get agents if they are logged in or a complete list. Parameters are optional.

        ---

        serializer: AgentDetailSerializer
        parameters:
            - name: is_available
              required: False
              type: boolean
            - name: agent_type
              required: False
              type: string
        """


        try:
            agent_type = [request.query_params["agent_type"]] if 'agent_type' in \
                                                               request.query_params else [type[0] for type in AGENT_TYPE]
            if "is_available" in request.query_params:
                agents = Agent.objects.filter(agent_type__in=agent_type,
                                              is_logged_in=request.query_params["is_available"])
            else:
                agents = Agent.objects.filter(agent_type__in=agent_type)
                
            return JSONResponse(AgentDetailSerializer(instance=agents, many=True).data, status=200)
        except (UnboundLocalError, KeyError) as e:
            beam(e)
            return JSONResponse({"Error":"No such active agents exist or parameters missing"}, status=200)


class AgentLogInView(APIView):
    # Endpoint: agent_login/
    def post(self, request):
        """

        summary: Logs a agent in to the system, returns token and other agent details.

        description: "headers: X-DEVICE-ID X-DEVICE-TYPE X-PUSH-ID"

        Auth: Doesn't require auth token in headers

        ---

        parameters:
            - name: contact_number
              required: true
              type: string
        """

        try:
            agents = Agent.objects.filter(contact_number= request.data["contact_number"])

            if len(agents):
                agent = agents[0]
                token = agent.login(headers=request.META)
                agent_json = AgentInfoSerializer(instance=agent).data
                agent_json["auth_token"] = token
                return JSONResponse(agent_json, status=200)
            else:
                return JSONResponse({"Message": "Invalid credentials!"}, status=403)

        except Exception as e:
            beam(e, request)
            return JSONResponse({"Message":"Could not authenticate user. Check your request headers"}, status=403)


class AgentLogOutView(APIView):
    # Endpoint: agent_logout/
    permission_classes = (AuthToken,)

    def delete(self, request):
        """

        summary: Logs out a user

        Auth: Needs the access token in the request header.

        ---

        """

        agent = request.user
        agent.is_logged_in = False
        agent.save()
        agent.update_delivery_boy(status=False)
        CRMAccessToken.objects.filter(access_token=request.META['HTTP_TOKEN']).update(is_active=False)

        AgentLog.objects.create(agent=agent, action="logout", created_on=django.utils.timezone.now())
        return JSONResponse({"Success": True}, status=200)


class CallView(APIView):
        # Endpoint: call/
        def date_handler(self, obj):
            return obj.isoformat() if hasattr(obj, 'isoformat') else obj

        def get(self, request):

            """


            summary: This endpoint is called by the ASMX server. It then publishes a push notification of
            the concerned agent on their cogent_agent_id

            Auth: Doesn't require the auth token in request headers

            ---

            serializer: CustomerDetailSerializer
            parameters:
                - name: contact_number
                  required: true
                  type: string
                - name: forwarded_for
                  required: true
                  type: string
                - name: agent_id
                  required: true
                  type: integer

            """

            client, order = None, False
            print 'request from asmx server', request.query_params
            # TODO  Check for a B2B customer
            try:
                forwarded_for = request.query_params["forwarded_for"]
                if forwarded_for in B2BCall.keys():
                    print forwarded_for
                # client = PhoneNumber.objects.filter(number= forwarded_for, number_type=NUMBER_TYPE[0][0])[0]
                from restaurant.models import Restaurant
                restaurant = Restaurant.objects.get(id=B2BCall[forwarded_for])
                client = ClientSerializer(instance=restaurant).data
            except (KeyError, IndexError):
                pass

            try:
                contact_number = request.query_params["contact_number"]
                customer = Customer.find_by_contact_number(contact_number= contact_number)

                customer_info = {"primary_number": contact_number, "name": "New Customer", "call_count": 1}

                if customer:
                    customer.call_count += 1
                    customer.save()
                    customer_info = CustomerDetailSerializer(instance=customer).data

                    order = customer.get_pending_order()

                    if order:
                        order = OrderDetailViewSerializer(instance=order).data

                    CallLog.log_call(customer, request.query_params["agent_id"])

                pusher = PushMessage(channel=request.query_params["agent_id"])
                print 'pusher order ', order
                pusher.push(json.dumps({
                    "client": client,
                    "customer": customer_info,
                    "order": order
                }, default=self.date_handler))


                return JSONResponse({"customer": customer_info}, status=200)
            except Exception as e:
                print traceback.format_exc(e)
                beam(e, request)
                return JSONResponse({"Success": False}, status=200)




class CustomerView(APIView):

    permission_classes = (AuthToken, )

    def get(self, request):
        """
        summary: Get a customers details by id or primary number

        ---

        serializer: CustomerDetailSerializer
        parameters:
            - name: customer_id
              required: false
              type: integer
            - name: primary_number
              required: false
              type: string
        """

        try:
            try:
                customer = Customer.objects.get(pk= request.query_params["customer_id"])
            except Exception as e:
                customer = Customer.objects.get(primary_number = request.query_params["primary_number"])

            customer_json = CustomerDetailSerializer(instance=customer)
            return JSONResponse(customer_json.data, status=200)
        except (ObjectDoesNotExist, KeyError) as e:
            beam(e)
            return JSONResponse({"Error": "Customer does not exist or you haven't provided the right parameters"}, status=400)

    def post(self, request):
        """


        summary: Add a new customer or update details of an existing one

        ---

        serializer: CustomerSerializer
        parameters:
            - name: primary_number
              required: false
              type: string
            - name: name
              required: true
              type: string
            - name: customer_id
              required: false
              type: integer


        """
        try:
            if "customer_id" in request.data:
                customer = Customer.objects.get(pk = request.data["customer_id"])
                customer.name = request.data["name"]
                customer.save()
                response = CustomerDetailSerializer(instance=customer).data
            else:
                customer = CustomerSerializer(data= request.data, partial=True)

                if customer.is_valid():
                    new_customer = customer.save()
                    response = CustomerDetailSerializer(instance=new_customer).data
                else:
                    response = customer.errors

            return JSONResponse(response, status=200)
        except Exception as e:
            beam(e)
            return JSONResponse({"Error": "Something went wrong"}, status=400)


class ContactNumberView(APIView):
    # Endpoint: contact_number

    permission_classes = (AuthToken, )

    def post(self, request):
        """

        summary:Adds a new contact number for the user

        ---

        serializer: ContactNumberSerializer

        parameters:
            - name: number
              required: true
              type: string
        """
        contact_numbers = ContactNumberSerializer(data= request.data, many=True)

        if contact_numbers.is_valid():
            contact_numbers.save()
            return JSONResponse({"Success": "Numbers saved"}, status=200)
        else:
            return JSONResponse(contact_numbers.errors, status=200)


class AddressView(APIView):
    # Endpoint: address/

    permission_classes =  (AuthToken,)

    def get(self, request):
        """

        summary: Fetch details of an address by id or get all adresses of a particular customer

        ---

        serializer: ContactNumberSerializer

        parameters:
            - name: address_id
              required: false
              type: string
            - name: customer_id
              required: false
              type: integer
        """

        many = True
        try:
            if 'address_id' in request.data:
                address = Address.objects.get(pk = request.query_params["address_id"])
                many = False
            else:
                address = Address.objects.filter(customer_id = request.query_params["customer_id"])

            return JSONResponse(AddressSerializer(instance=address, many=many).data, status=200)
        except (KeyError, ObjectDoesNotExist, UnboundLocalError) as e:
            beam(e)
            return JSONResponse({"Error": "Address does not exist/Bad request"}, status=200)

    def post(self, request):
        """

        summary: Add or update address, partial update works as well, allowing you to send just the id and the updated field

        ---
        serializer: AddressViewSerializer

        parameters:
            - name: address_id
              required: false
              type: integer
            - name: complete_address
              required: true
              type: string
            - name: locality
              required: true
              type: integer
            - name: city
              required: true
              type: integer
            - name: address_type
              required: true
              type: string

        """
        if 'address_id' in request.data:

            address= Address.objects.get(pk= request.data["address_id"])
            updated_serializer = AddressSerializer(address, data=request.data, partial=True)
            if updated_serializer.is_valid():
                address = updated_serializer.save()
                return JSONResponse(AddressViewSerializer(instance=address).data, status=200)
            else:
                return JSONResponse(updated_serializer.errors, status=400)
        else:
            addresses = AddressSerializer(data=request.data)

            if addresses.is_valid():
                address = addresses.save()

                return JSONResponse(AddressViewSerializer(instance=address).data, status=200)
            else:
                return JSONResponse(addresses.errors, status=400)


class CityView(APIView):
    # /city
    def get(self, request):
        """

        summary: Get list of all cities

        Auth: Doesn't require authentication token in headers

        ---

        serializer: CitySerializer

        """
        cities = City.objects.all()

        return JSONResponse(CitySerializer(instance= cities, many= True).data, status= 200)


class LocalityView(APIView):

    def get(self, request):
        """

        summary: Get list of all localities for a particular city or get latitude/longitude of a given locality
        or get a list of all localities

        Auth: Doesn't require authentication token in headers

        ---

        serializer: LocalitySerializer

        parameters:
            - name: city_id
              required: false
              type: integer
            - name: locality_id
              required: false
              type: integer
        """

        if "city_id" in request.query_params:
            city = City.objects.get(pk= request.query_params["city_id"])
            localities = Locality.objects.filter(city= city)
            return JSONResponse(LocalitySerializer(instance= localities, many=True).data, status=200)
        elif "locality_id" in request.data:
            locality = Locality.objects.filter(name=request.query_params["locality_id"])
            return JSONResponse(LocalitySerializer(instance=locality).data, status=200)
        else:
            localities = Locality.objects.all()
            return JSONResponse(LocalitySerializer(instance= localities, many=True).data, status=200)


class OrderView(APIView):
    #endpoint: order/
    permission_classes = (AuthToken,)

    def get(self, request):

        """

        summary: Returns list of orders by id, status or restaurant_id and status

        ---
        serializer: OrderViewSerializer

        parameters:
            - name: order_id
              required: false
              type: integer
            - name: status
              required: false
              type: string
            - name: resturant_id
              required: false
              type: integer

        """
        try:
            many = True
            if "order_id" in request.query_params:
                orders = Order.objects.get(pk=request.query_params["order_id"])
                many = False
            elif "status" in request.query_params:
                start_index = request.query_params.get("start_index", 0)
                end_index = request.query_params.get("end_index", 100)
                if request.query_params["status"] == 'all':
                    orders = Order.objects.all().order_by('-created_on')[start_index:end_index]
                elif request.query_params["status"] == 'delayed':
                    orders = Order.objects.filter(delayed_by__gt=0).exclude(status__in=['delivered', 'cancelled']).order_by('-created_on')[start_index:end_index]
                else:
                    orders = Order.objects.filter(status=request.query_params["status"]).order_by('-created_on')[start_index:end_index]
            elif "restaurant_id" in request.query_params:
                orders = Order.objects.filter(restaurant_id= request.query_params["restaurant_id"],
                                              status= request.query_params["status"]).order_by("-created_on")

            try:
                if request.META["HTTP_X_DEVICE_TYPE"] == 'android':
                    return JSONResponse(OrderViewSerializer(instance=orders).data, status=200)
            except:
                pass
            return JSONResponse(OrderDetailViewSerializer(instance=orders, many=many).data, status=200)

        except (ObjectDoesNotExist, KeyError, UnboundLocalError) as e:
            beam(e, request)
            return JSONResponse({"Error": "No such order exists or bad request with missing parameters"}, status=400)

    def post(self, request):

        """
        - Creates order on request of operator
        - order is in Awaiting confirmation state
        - Delivery has not been created till now.
        - Agent gets an estimated delivery time based on google api request

        ---
        serializer: OrderCreateSerializer

        parameters:
            - name: agent
              required: true
              type: integer
            - name: customer
              required: true
              type: integer
            - name: address
              required: true
              type: integer
            - name: order_items
              pytype: OrderLineItemOrderCreateSerializer
              paramType: body
            - name: restaurant
              required: true
              type: integer
            - name: total_amount
              required: true
              type: decimal
            - name: service_tax
              required: false
              type: decimal
            - name: vat_tax
              required: false
              type: decimal
            - name: discount_amount
              required: false
              type: integer
            - name: other_charges
              required: false
              type: integer


        responseMessages:
            - code: 200
            - message: order details

        """
        request.data["agent"] = request.user

        try:
            if "id" in request.data:
                order = Order.objects.get(pk = request.data["id"])
                edited_order = OrderCreateSerializer()
                order = edited_order.update(instance= order, validated_data=request.data)
                # TODO - Completely manual updation of restaurant and delivery boy, raise ticket later

                try:
                    order.delivery_boy.notify({"order_id": order.id, "status": "updated"})
                except Exception:
                    pass

                OrderLog.objects.create(order=order, message= "Order updated",
                                    owner_id= request.user.id, owner_type= "Agent")
                response = OrderDetailViewSerializer(instance=order).data
            else:
                order = OrderCreateSerializer()
                new_order = order.create(request.data)
                OrderLog.objects.create(order=new_order, message= dict(STOCK_MESSAGES)["awaiting_confirmation"],
                                    owner_id= request.user.id, owner_type= "Agent")
                new_order.notify_restaurant()
                # Figure out if we need to call the restaurant and raise a ticket for that
                response = new_order.should_call()
                response["order"] = OrderDetailViewSerializer(instance=response["order"]).data

            return JSONResponse(response, status=200)
        except Exception as e:
            beam(e)
            return JSONResponse({"Error": "Order creation failed"}, status=400)


class OrderLogView(APIView):

    # Endpoint: order_log/
    permission_classes = (AuthToken, )

    def post(self, request):
        """

        summary: Post a new comment for the order log

        ---

        parameters:
            - name: order
              required: true
              type: integer
            - name: assigned_to
              required: true
              type: integer
            - name: created_by
              required: true
              type: integer
            - name: ticket_type
              required: true
              type: string
            - name: details
              requireds: true
              type: string
        """

        log = OrderLogSerializer(data= request.data)

        if log.is_valid():
            log.save()
            return JSONResponse({"Success": "Your comment has been noted!"}, status=200)
        else:
            return JSONResponse(log.errors, status=200)


class DeliveryView(APIView):
    # Endpoint: /delivery
    permission_classes = (AuthToken,)

    def post(self, request):

        """
        summary: Reassign a delivery to a new delivery boy

        ---

        parameters:
            - name: order_id
              required: true
              type: integer
            - name: delivery_boy_id
              requireds: true
              type: integer

        """

        try:
            order = Order.objects.get(pk=request.data["order_id"])
            boy = DeliveryBoy.objects.get(pk= request.data["delivery_boy_id"], is_available=True)

            if boy:
                order.assign_delivery_boy(delivery_boy=boy)
                OrderLog.objects.create(order=order, message=dict(STOCK_MESSAGES)["assigned_manually"],
                                    owner_id= request.user.id, owner_type="oe")
                boys = DeliveryBoy.objects.filter(is_available=True,
                                              agent__in= Agent.objects.filter(is_logged_in= True))
                boys_json = DeliveryBoyDetailSerializer(instance=boys, many=True).data

                if boy.notify(payload={"order_id": order.id, "status": "new"}):
                    return JSONResponse({"Success": "Delivery boy was assigned and notified successfully", "boys": boys_json},
                                        status=200)
                else:
                    return JSONResponse({"Success": ("Delivery boy assigned but failed to notify."
                                                    " Please do it via call - %s" % boy.agent.contact_number), "boys": boys_json},
                                        status=200)
            else:
                return JSONResponse({"Message": "Delivery boy not available anymore"}, status=403)

            # if order.delivery_boy:
            #     order.delivery_boy.is_available = request.data["make_available"]

        except Exception as e:
            beam(e)
            return JSONResponse({"Error": "Order does not exist or delivery boy is not available anymore"},
                                status=400)

    def put(self, request):

        """
        summary: Delivery boy or oe sets the order status and other optional details

        description: "Used by the android app this allows delivery boy to update the status of an order
        and pass additional attributes such as bill image, money paid to restaurant or customer"

        ---

        parameters:
            - name: order_id
              required: true
              type: integer
            - name: delivery_status
              required: true
              type: string
            - name: amount_paid
              required: false
              type: integer
            - name: payment_card
              required: false
              type: integer
            - name: payment_cash
              required: false
              type: integer


        """
        try:
            valid_status = [key for key in dict(DELIVERY_STATUS)]



            # Check if required parameters are available
            if request.data["delivery_status"] in valid_status:

                order = Order.objects.get(id=request.data['order_id'])

                order.status = request.data["delivery_status"]
                if order.status == 'pick_up':
                    # try:
                    #     seconds = GoogleMaps().estimate_delivery_time(order.address, (order.restaurant.latitude,
                    #                                                                   order.restaurant.longitude))
                    # except Exception as e:
                    seconds = 15*60

                    order.expected_delivery_time = datetime.datetime.now() + datetime.timedelta(seconds=seconds)
                    order.restaurant_amount = request.data["amount_paid"]

                elif order.status == 'delivered':
                    order.delivered_at = datetime.datetime.now()
                    order.payment_card = request.data["payment_card"]
                    order.payment_cash = request.data["payment_cash"]
                    order.deassign_delivery_boy()
                    try:
                        order.discount_on_delivery = request.data["discount_on_delivery"]
                        order.address.latitude = request.data["latitude"]
                        order.address.longitude = request.data["longitude"]
                        order.address.save()
                    except KeyError as k:
                        pass

                elif order.status == 'cancelled':
                    order.deassign_delivery_boy()

                order.save()

                OrderLog.objects.create(order=order, message=dict(STOCK_MESSAGES)[order.status],
                                        owner_id= request.user.id, owner_type=request.user.agent_type)

                return JSONResponse({"Success": True}, status=200)

            else:
                return JSONResponse({"Error": "Either order id is missing or you have provided a non standard status."}, status=400)
        except Exception as e:
            print traceback.format_exc(e)
            beam(e)
            return JSONResponse({"Error": "Something went wrong"}, status=400)


class DeliveryBoyView(APIView):

    #Endpoint: delivery_boy/

    permission_classes = (AuthToken,)

    def get(self, request):

        """

        summary: Get a list of all active or available delivery boys

        ---

        serializer: DeliveryBoyDetailSerializer

        parameters:
            - name: status
              required: true
              type: string

        """
        status = request.query_params["status"]
        if status == 'all_available':
            boys = DeliveryBoy.objects.filter(agent__in= Agent.objects.filter(is_logged_in= True), is_available= True)
        else:
            boys = DeliveryBoy.objects.filter(agent__in= Agent.objects.filter(is_logged_in= True))

        json_boys = DeliveryBoyDetailSerializer(instance=boys, many=True)

        return JSONResponse(json_boys.data, status=200)

    def post(self, request):

        """

        summary: Post lat/long of a delivery boy

        ---

        serializer: DeliveryBoyLocationSerializer

        parameters:
            - name: delivery_boy
              required: true
              type: integer
            - name: latitude
              required: true
              type: decimal
            - name: longitude
              required: true
              type: decimal
        """

        try:
            request.data["delivery_boy"] = DeliveryBoy.objects.filter(agent= request.user)[0]

            location = DeliveryBoyLocationSerializer()
            location.create(validated_data=request.data)

            return JSONResponse({'Success': 'location logged successfully'})
        except KeyError as e:
            beam(e)
            return JSONResponse({"Error": "No delivery boy found"}, status=200)


class TicketView(APIView):

    # Endpoint: ticket/
    permission_classes = (AuthToken,)

    def get(self, request):

        """

        summary: Get tickets for a particular order, type or all tickets in the system which are active

        ---

        serializer: TicketSerializer

        parameters:
            - name: order_id
              required: false
              type: integer
            - name: ticket_type
              required: false
              type: string
        """
        try:
            if 'order_id' in request.query_params:
                tickets = Ticket.objects.filter(order_id= request.query_params["order_id"], is_active=True)
            elif 'ticket_type' in request.query_params :
                tickets = Ticket.objects.filter(ticket_type = request.query_params['ticket_type'])
            else:
                tickets = Ticket.objects.filter(is_active=True)

            return JSONResponse(TicketViewSerializer(instance=tickets, many=True).data, status=200)
        except UnboundLocalError as e:
            beam(e)
            return JSONResponse({"Error": "Missing parameters in request"}, status=400)

    def post(self, request):
        """

        summary: Create or update a new ticket

        ---

        serializer: TicketSerializer

        parameters:
            - name: ticket_id
              required: false
              type: integer
            - name: details
              required: false
              type: string
            - name: assigned_to
              type: integer
              required: false
        """

        ticket_serializer = TicketSerializer(data= request.data, partial=True)

        try:
            ticket = Ticket.objects.get(pk= request.data["ticket_id"])
            ticket_serializer.instance = ticket
        except KeyError:
            pass

        if ticket_serializer.is_valid():
            ticket = ticket_serializer.save()
            return JSONResponse(TicketViewSerializer(instance=ticket).data, status=200)
        else:
            return JSONResponse(ticket_serializer.errors, status=400)

    def put(self, request):
        """

        summary: Change the status of a ticket

        ---

        parameters:
            - name: ticket_id
              required: false
              type: integer
            - name: is_active
              required: false
              type: boolean
        """

        try:
            is_active = request.data["is_active"]
            ticket_id = request.data["ticket_id"]

            resolved_at = datetime.datetime.now() if not is_active else None

            Ticket.objects.filter(id= ticket_id).update(is_active=is_active, resolved_at=resolved_at)
            return JSONResponse({"Success": "Ticket status updated"}, status=200)
        except (KeyError, ObjectDoesNotExist) as e:
            beam(e)
            return JSONResponse({"Error": "Missing parameters"}, status=400)


class BillImage(APIView):

    # parser_classes = (MultiPartParser, )
    # permission_classes = (AuthToken, )

    def put(self, request):
        """
        summary: Upload end point for bill image

        ---

        parameters:
            - name: order_id
              required: true
              type: integer
            - name: bill_image
              required: true
              type: file
        """

        try:
            order = Order.objects.get(id=request.data["order_id"])
            order.bill_image = request.FILES["bill_image"]
            order.save()
            return JSONResponse({"Success": "Image uploaded"}, status=200)

        except Exception, e:
            beam(e)
            return JSONResponse({"Error": 'Invalid response'}, status=400)


class CallBackRequestView(APIView):

    def post(self, request):

        try:
            requested_by = request.data["requested_by"] if "requested_by" in request.data else ''

            callback = CallBackRequest(contact_number=request.data["contact_number"], requested_by=requested_by)

            callback.save()

            cosmocall_request(contact_number=request.data["contact_number"])

            return JSONResponse({"Message": "Callback request created"}, status=200)
        except Exception as e:
            beam(e)
            return JSONResponse({"Message": "Something went wrong!"}, status=400)


class ContactUsView(APIView):

    def post(self, request):
        from django.core.mail import send_mail
        try:
            name = request.data["name"]
            from_email = request.data["from_email"]
            message = "%s \n\nName: %s" % (request.data["message"], name)
            send_mail(subject="Contact Us", message=message, from_email=from_email,
                      recipient_list=['varun@refiral.com', 'varunj.dce@gmail.com',
                                      'anmol.batra@refiral.com', 'rajat.goel@refiral.com'],
                      fail_silently=False)
            return JSONResponse({"message": "Thanks for getting in touch! We will get back to you shortly."}, status=200)
        except Exception as e:
            beam(e)


from django import forms


class UploadFileForm(forms.Form):

    bill_file  = forms.FileField()

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseNotAllowed, HttpResponseServerError


@csrf_exempt
def upload_file(request):
    #print request.FILES
    # form = UploadFileForm(request.POST, request.FILES)
    # if not form.is_valid():
    #     return HttpResponseServerError("Invalid call")

    #handle_uploaded_file(request.FILES['file'])
    print request.POST["order_id"]
    order = Order.objects.get(id=request.POST['order_id'])
    order.bill_image = request.FILES['bill_image']
    order.save()
    return HttpResponse('OK')

# class DeliveryBoyTracking(APIView):
#
#     def get(self, request):
#
#         delivery_boy_id =