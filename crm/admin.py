from django.contrib import admin
from crm.models import OrderLineItem, Order, Customer, Address, City, Locality, Agent, DeliveryBoy, OrderLog, Ticket
from import_export.admin import ImportExportModelAdmin
from import_export import resources


class CustomerResource(resources.ModelResource):
    class Meta:
        model = Customer


class CustomerAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = CustomerResource


class OrderResource(resources.ModelResource):
    class Meta:
        model = Order


class OrderAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = OrderResource


class AddressResource(resources.ModelResource):
    class Meta:
        model = Address


class AddressAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = AddressResource


class LocalityResource(resources.ModelResource):
    class Meta:
        model = Locality


class LocalityAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = LocalityResource


class CityResource(resources.ModelResource):
    class Meta:
        model = City


class CityAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = CityResource


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderLineItem)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Address, AddressAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Locality, LocalityAdmin)
admin.site.register(Agent)
admin.site.register(DeliveryBoy)
admin.site.register(OrderLog)
admin.site.register(Ticket)