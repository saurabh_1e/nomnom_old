DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
     'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'nomnom',
        'HOST': 'nomnom.cstcmgibmngs.ap-southeast-1.rds.amazonaws.com',
        'PORT': '3306',
        'USER': 'nommer',
        'PASSWORD': 'n0mN0m.it',
    }
}

ELASTIC_SEARCH = {

    'HOST': 'http://54.254.195.155:9200/',
}

PUSHER = {
    "HOST": "http://api.gonomnom.in:9080/",

}
