from crm.models import Order, OrderLog, Alarm
from datetime import datetime, timedelta
from crm.serializers import OrderDetailViewSerializer
from nomnom.choices import *
from utilities.push_message import PushMessage
from utilities.raygun import beam

import json
import pytz

# time window for each of the delivery status to live
window = {
    "awaiting_confirmation": 2,
    "confirmed": 2,
    "assigned": 2,
    "delivery_confirmed": 8,
    "arrived": 8,
    "pick_up": 8,
    "reached": 3
}


def watchdog():

    try:
        orders = Order.objects.exclude(status__in=['delivered', 'cancelled'])
        delayed_orders = []
        ontrack_orders = []
        pusher = PushMessage(channel="order_list")

        for order in orders:
            # Get expected time for the current status to have changed
            expected_time = window[order.status]

            log = OrderLog.objects.filter(order=order, message=dict(STOCK_MESSAGES)[order.status])

            if len(log):
                assigned_time = log[0].created_on

                #Time aware timestamp fubar
                current_time = datetime.utcnow().replace(tzinfo=pytz.utc)

                if current_time - assigned_time > timedelta(minutes=expected_time):

                    delayed_by = int((current_time - assigned_time).total_seconds()) - (expected_time*60)

                    #If an alarm exists for the current status update the delay time otherwise create a new alarmnom
                    alarm = Alarm.objects.get_or_create(order=order, for_status=order.status)
                    alarm[0].delayed_by = delayed_by
                    alarm[0].save()

                    #Set order delayed time for "current step"
                    order.delayed_by = delayed_by
                    order.save()

                    delayed_orders.append(order)

                    order = OrderDetailViewSerializer(instance=order).data

                    pusher.push(json.dumps(order))
                    print "Order delayed \n"

                else:
                    ontrack_orders.append(order)
                    print "Order on track \n"

        orders = delayed_orders + ontrack_orders

        return orders

    except Exception as e:
        beam(e)




