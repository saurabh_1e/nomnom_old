ADDRESS_TYPE = (('work', 'WORK'), ('home', 'HOME'))

AGENT_TYPE = (
    ("agent", "Call agent"),
    ("system", "System"),
    ("oe","Operations Executive"),
    ("delivery_boy","Delivery Boy")
)

BONE = (
    ('na', 'Not applicable'),
    ('bone', 'Bone'),
    ('boneless', 'Boneless'),
)

CART_CATEGORY = (
    ('appetizer', 'Appetizer'),
    ('main_course', 'Main Course'),
    ('rice_bread', 'Rice & Bread'),
    ('side_dish', 'Side Dish'),
    ('desserts', 'Desserts'),
    ('combos', 'Combos'),
    ('beverages', 'Beverages'),
)

COST_FOR_TWO = (
    ("0-250", "0-250"),
    ("250-600", "250-600"),
    ("600-1200", "600-1200"),
    ("1200-2500", "1200-2500")
)

CUISINES = (
    ('North Indian', 'North Indian'),
    ('chinese', 'Chinese'),
    ('South Indian', 'South Indian'),
    ('italian', 'Italian'),
    ('continental', 'Continental'),
    ('thai', 'Thai'),
    ('mexican', 'Mexican'),
    ('lebanese', 'Lebanese')
)


CUSTOMER_CATEGORY = (('rich', 'Rich'),)

DELIVERY_STATUS = (
    ('awaiting_confirmation', 'Awaiting Confirmation'),
    ('confirmed', 'Confirmed'),
    ('assigned', 'Assigned'),
    ('delivery_confirmed', 'Delivery Confirmed'),
    ('arrived', 'Arrived'),
    ('pick_up', 'Picked Up'),
    ('reached', 'Reached Destination'),
    ('delivered', 'Delivered'),
    ('cancelled', 'Cancelled')
)

DELIVERY_TIME = (
    ("0-30", "Less than 30 min."),
    ("30-45", "30 - 45 min."),
    ("45-120", "More than 50 min.")
)

FAMOUS_FOR = (
    ('desserts', 'Desserts'),
    ('fried', 'Fried'),
    ('grilled', 'Grilled'),
    ('healthy', 'Healthy'),
    ('non_veg', 'Non Vegetarian'),
    ('sea_food', 'Sea Food'),
    ('snacks', 'Snacks'),
    ('spicy_food', 'Spicy Food'),
    ('street_food', 'Street Food')
)

FINAL_COOKING = (
    ('na','Not applicable'),
    ('roasted', 'Roasted'),
    ('grilled', 'Grilled'),
    ('baked', 'Baked'),
    ('poached','Poached'),
    ('cooked', 'Cooked'),
    ('raw', 'Raw'),
    ('boiled', 'Boiled'),
    ('steamed', 'Steamed'),
    ('deep_fry', 'Deep Fry'),
    ('pan_fry', 'Pan Fry'),
    ('stir_fry', 'Stir Fry'),
    ('shallow_fry', 'Shallow Fry'),
    ('wok_cooked', 'Wok Cooked')
)

FOOD_DISH_TYPE = (
    ('vegetarian', 'Vegetarian'),
    ('non_vegetarian', 'Non-Vegetarian'),
    ('sea_food', 'Sea Food'),
    ('vegan', 'Vegan'),
    ('gluten_free', 'Gluten Free'),
    ('eggetarian', 'Eggetarian')
)

FOOD_OPTIONS = (
    ('non_vegetarian', 'Non Vegetarian'),
    ('pure_vegetarian', 'Pure Vegetarian'),
    ("vegetarian", "Vegetarian"),
    ('seafood', 'Sea food')
)

FRY = (
    ('na', 'Not Applicable'),
    ('deep_fried', 'Deep Fried'),
    ('pan_fried', 'Pan Fried'),
    ('stir_fried', 'Stir Fried'),
    ('shallow_fried', 'Shallow Fried'),

)

GRAVY = (
    ('na', 'Not applicable'),
    ('dry', 'Dry'),
    ('semi_dry', 'Semi Dry'),
    ('gravy', 'Gravy')
)

GENDER = (('male', 'MALE'), ('female', 'FEMALE'))

INITIAL_COOKING = (

    ('na','Not Applicable'),
    ('marinated', 'Marinated'),
    ('boiled', 'Boiled'),
    ('steamed', 'Steamed'),
    ('mashed', 'Mashed'),
    ('roasted', 'Roasted'),
    ('grilled', 'Grilled'),
    ('baked', 'Baked'),
    ('cooked', 'Cooked'),
    ('deep_fry', 'Deep Fry'),
    ('pan_fry', 'Pan Fry'),
    ('stir_fry', 'Stir Fry'),
    ('shallow_fry', 'Shallow Fry'),
    ('raw', 'Raw'),
    ('wok_cooked', 'Wok Cooked')
)

LANGUAGES = (('english', 'ENGLISH'), ('hindi', 'HINDI'))

NUMBER_TYPE = (
    ('b2b_client', 'B2B Client Number'),
    ('sms', 'SMS Number'),
    ('order_placement', 'Order placement')
)

OIL_CONTENT = (
    ('na', 'Not Available'),
    ('non_oily', 'Non Oily'),
    ('low_oily', 'Low Oily'),
    ('medium_oily', 'Medium Oily'),
    ('highly_oily', 'High Oily')
)

PAYMENT_PREFERENCES = (('cash', 'Cash'), ('card', 'Card'))

PORTION = (
    ('quarter', 'Quarter'),
    ('half','Half'),
    ('full', 'Full'),
    ('small', 'Small'),
    ('regular', 'Regular'),
    ('medium', 'Medium'),
    ('large', 'Large'),
    ('extra_large', 'Extra Large'),
    ('bowl', 'Bowl'),
    ('cup', 'Cup')
)

PREP_TIME = (
    ("0-30", '< 30 min.'),
    ("30-45", '30-45 min.'),
    ("45-60", '45-60 min.'),
    ("60-120", '> 60 min.')
)

PRICE = (
    ("0-200", '< 200'),
    ("200-400",'200-400'),
    ("400-700", '400-700'),
    ("700-2500", '> 700')
)

SERVING_OPTION = (
    ('hot', 'Hot'),
    ('chilled', 'Chilled')
)

SERVING_SIZE = (
    (1, 'One'),
    (2, 'Two'),
    (3, 'Three'),
    (4, 'Four')
)

SPECIAL = (
    ('comfort_food', 'Comfort Food'),
    ('stoner_food', 'Stoner Food'),
    ('munchies', 'Munchies')
)


SPICY = (
    ('na', 'Not Applicable'),
    ('non_spicy', 'Non Spicy'),
    ('less_spicy', 'Less Spicy'),
    ('regular_spicy', 'Normal'),
    ('very_spicy', 'Very Spicy'),
)

STOCK_MESSAGES = (
    ("awaiting_confirmation", "Order created"),
    ("failed_to_notify", "Failed to notify restaurant via SMS"),
    ("confirmed", "Restaurant confirmed order"),
    ("assigned", "Delivery boy assigned"),
    ("assigned_manually", "Delivery boy assigned manually"),
    ("delivery_confirmed", "Delivery Confirmed"),
    ("arrived", "Delivery boy arrived"),
    ("pick_up", "Delivery boy picked up order"),
    ("reached", "Delivery boy reached destination"),
    ("delivered", "Delivery boy finished delivery"),
    ("delivery_boy_notified", "Delivery boy has been assigned and notified of the order"),
    ("failed_to_assign_delivery_boy", "Failed to assign delivery boy"),
    ("failed_to_notify_delivery_boy", "Failed to notify delivery boy"),
    ('delivery_boy_failed_to_respond', 'Delivery Boy failed to respond'),
    ('waiting_to_notify_delivery_boy', 'Waiting to notify delivery boy'),
    ('cancelled', 'Order cancelled')
)

TASTE = (

    ('sweet', 'Sweet'),
    ('sour', 'Sour'),
    ('bitter', 'Bitter'),
    ('sweet_sour', 'Sweet & Sour'),
    ('bitter_sweet', 'Bitter & Sweet'),
    ('tangy', 'Tangy'),
    ('bland', 'Bland'),
    ('general', 'General    ')
)

TICKET_TYPE = (
    ('place_order', 'Place Order via call'),
    ('no_way', 'No way to communicate order to restaurant'),
    ('failed_to_notify', 'Failed to notify restaurant'),
    ("restaurant_not_accepting", "Restaurant has not accepted order"),
    ("delivery_boy_not_assigned", "No delivery boy has been assigned to this delivery"),
    ('order_delayed', 'Order delayed'),
    ('order_items_unavailable', 'Order items not available'),
    ('failed_to_notify_delivery_boy', 'Failed to notify delivery boy'),
    ('no_delivery_boy_available', 'No delivery boy is available'),
    ("delivery_boy_not_accepting", "Delivery boy not accepting order"),
    ('other', 'Other')
)

INGREDIENT = (
    ('paneer', 'Paneer'),
    ('soya', 'Soya'),
    ('chicken', 'Chicken'),
    ('mutton', 'Mutton'),
    ('lamb', 'Lamb'),
    ('beef', 'Beef'),
    ('egg', 'Egg'),
    ('pork', 'Pork'),
    ('prawn', 'Prawn')

)
VARIETY = (
    ('na', 'Not Applicable'),
    ('chicken', 'Chicken'),
    ('prawns', 'Prawn'),
    ('pork', 'Pork'),
    ('beef', 'Beef'),
    ('egg', 'Egg'),
    ('paneer', 'Paneer'),
    ('lamb', 'Lamb'),
    ('fish', 'Fish'),
    ('mushroom', 'Mushroom'),
    ('black mushroom', 'Black Mushroom'),
    ('soya', 'Soya'),
    ('kofta', 'Kofta'),
    ('tofu', 'Tofu'),
    ('sweet', 'Sweet'),
    ('sour', 'Sour'),
    ('sweet sour', 'Sweet & Sour')
)

B2BCall = \
    {'4286751': 26,  '4286752': 50, '4286753': 58, '4286754': 57,
     '4286755': 70, '4286757': 'NomNom'}