from nomnom.settings.common import PUSHER
from utilities.raygun import beam

import requests


class PushMessage:
    def __init__(self, channel, hostname=PUSHER["HOST"]):
        self.hostname = hostname
        self.channel = channel

    def push(self, message):
        url = self.get_base_url() + self.channel
        try:
            response = requests.post(url, message, headers={"content-type": "application/json"})
            try:
                if response.status_code != 200:
                    raise Exception("Status code %s" % str(response.status_code))
            except Exception as e:
                beam(e)
        except Exception as e:
            beam(e)

    def delete(self):
        url = self.get_base_url() + self.channel
        requests.delete(url)

    def get_base_url(self):
        return self.hostname + 'pub?id=/'
