import requests
import multiprocessing
import traceback

from utilities.raygun import beam


def cosmocall_request(contact_number):

    thread_one = multiprocessing.Process(target=callback_request, args=(contact_number,))
    thread_one.start()

    # Wait for 10 seconds or until process finishes
    thread_one.join(4)

    #If thread is still active
    if thread_one.is_alive():
        print "running... let's kill it..."

        # Terminate
        thread_one.terminate()
        thread_one.join()
    else:
        print 'Thread done! Bi Bi!'


def callback_request(contact_number):

    try:
        xml_body = """<?xml version="1.0" encoding="utf-8"?>
                        <soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                        xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                            <soap:Body>
                                 <SendMail xmlns="http://tempuri.org/">
                                        <MobNo>{contact_number}</MobNo>
                                </SendMail>
                            </soap:Body>
                        </soap:Envelope>""".format(contact_number=contact_number)
        headers = {
                    "Host":             "14.141.248.156",
                    "Content-Type":     "text/xml; charset=UTF-8",
                    "Content-Length":    len(xml_body),
                    "SOAPAction":        "http://tempuri.org/SendMail"
                   }
        response = requests.post('http://14.141.248.156/WSMail/WebService.asmx',
                                 xml_body,
                                 headers=headers)

        print response.text
    except Exception as e:
        #beam(e)
        print traceback.format_exc(e)
        print None
