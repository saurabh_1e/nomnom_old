from nomnom.settings.common import GOOGLE_MAPS_KEY
import googlemaps


class GoogleMaps(object):

    @staticmethod
    def __connect():

        return googlemaps.Client(key=GOOGLE_MAPS_KEY)

    def __distance_calculate(self, point1, point2):

        gmaps = self.__connect()
        distance = gmaps.distance_matrix(point1, point2)
        return distance

    @staticmethod
    def __location_points(address, pickup_latlon):

        destination = str(address.locality) + ', ' + str(address.city)
        restaurant_location = ",".join(map(str, pickup_latlon))

        return destination, restaurant_location

    def estimate_delivery_time(self, delivery_address, pickup_latlon):

        point1, point2 = self.__location_points(delivery_address, pickup_latlon)
        distance = self.__distance_calculate(point1, point2)
        return int(distance['rows'][0]['elements'][0]['duration']['value'])
