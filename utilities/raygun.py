from raygun4py import raygunprovider
from nomnom.settings import common
import sys

ray = raygunprovider.RaygunSender(common.RAYGUN4PY_API_KEY)


def beam(e, request=None):
    err = sys.exc_info()

    if request:
        headers = {
            "referer": request.META["HTTP_REFERER"],
            "user-Agent": request.META["HTTP_USER_AGENT"]
        }
        request_data = {
            "headers": headers,
            "hostName": request.META["HTTP_HOST"],
            "url": request.META["PATH_INFO"],
            "httpMethod": request.META["REQUEST_METHOD"],
            "ipAddress": "1.0.0.1",
            "queryString": dict(request.query_params.iterlists()) if request.query_params else None,
            "form": None,
            "rawData": dict(request.data.iterlists()) if request.data else None
        }
    #Print for debugging purpose
        print ray.send_exception(exception=e, exc_info=err, httpRequest=request_data)

    else:
        print e, err


