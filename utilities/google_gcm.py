import gcm
from nomnom.settings.common import GOOGLE_GCM_KEY
from utilities.raygun import beam

class GCM():

    def __init__(self, api_key=GOOGLE_GCM_KEY):
        self.gcm = gcm.GCM(api_key)

    def send_message(self, gcm_ids, data):
        try:
            print self.gcm.json_request(registration_ids=gcm_ids, data=data)
        except Exception as e:
            beam(e)
