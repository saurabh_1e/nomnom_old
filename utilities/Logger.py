


class Logger(object):

    @staticmethod
    def log_ticket(order, message, owner, owner_type, ticket_type):
        from crm.models import OrderLog, Ticket
        OrderLog.objects.create(order=order, message=message, owner_type=owner_type, owner_id=owner.id)
        ticket= Ticket.objects.create(created_by=order.agent, assigned_to=owner, ticket_type=ticket_type, order=order)
        return ticket.id

