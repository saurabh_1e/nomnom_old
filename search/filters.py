from nomnom.choices import *
from collections import OrderedDict
from search.models import MenuFilters
from restaurant.models import Restaurant, Brand, Dish, DishVariation

import json

restaurant_filters = [
    {
        "filter_name": "primary_cuisine",
        "display_name": "Cuisines",
        "color": "#B45678",
        "values": sorted(CUISINES),
        "doc_type": "restaurant",
        "boost": 1,
        "type": "term"
    },
    {
        "filter_name": "food_type",
        "display_name": "Veg/Non-Veg",
        "color": "#B45678",
        "values": sorted(FOOD_OPTIONS),
        "doc_type": "restaurant",
        "boost": 2,
        "type": "term"
    },
    {
        "filter_name": "ingredients",
        "display_name": "Key Ingredient",
        "color": "#B45678",
        "values": sorted(INGREDIENT),
        "doc_type": "restaurant",
        "boost": 2,
        "type": "term"

    },
    {
        "filter_name": "famous_for",
        "display_name": "Known for",
        "color": "#B45678",
        "values": sorted(FAMOUS_FOR),
        "doc_type": "restaurant",
        "boost": 1,
        "type": "term"
    },
    {
        "filter_name": "estimated_preparation_time",
        "display_name": "ETA",
        "color": "#B45678",
        "values": sorted(PREP_TIME),
        "doc_type": "restaurant",
        "boost": 1,
        "type": "range"
    },

    {
        "filter_name": "cost_for_two",
        "display_name": "Cost for two",
        "color": "#B45678",
        "values": sorted(COST_FOR_TWO),
        "doc_type": "restaurant",
        "boost": 1,
        "type": "range"
    },
]
  #  {
    #     "filter_name": "dish_category",
    #     "display_name": "Cuisines",
    # }
    # {
    #     "filter_name": "food_type",
    #     "display_name": "Food dish type",
    #     "color": "#123456",
    #     "values": sorted(FOOD_DISH_TYPE),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "gravy_type",
    #     "display_name": "Gravy Type",
    #     "color": "#123456",
    #     "values": sorted(GRAVY),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "has_bone",
    #     "display_name": "Bone",
    #     "color": "#123456",
    #     "values": sorted(BONE),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "spicy_type",
    #     "display_name": "Spicy",
    #     "color": "#123456",
    #     "values": sorted(SPICY),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "final_cooking",
    #     "display_name": "Final Cooking",
    #     "color": "#123456",
    #     "values": sorted(FINAL_COOKING),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "taste",
    #     "display_name": "Taste",
    #     "color": "#123456",
    #     "values": sorted(TASTE),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "oil_content",
    #     "display_name": "Oil",
    #     "color": "#123456",
    #     "values": sorted(OIL_CONTENT),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "frying_type",
    #     "display_name": "Fry",
    #     "color": "#123456",
    #     "values": sorted(FRY),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "specials",
    #     "display_name": "Specials",
    #     "color": "#123456",
    #     "values": sorted(SPECIAL),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "initial_cooking",
    #     "display_name": "Initial cooking",
    #     "color": "#123456",
    #     "values": sorted(INITIAL_COOKING),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # },
    # {
    #     "filter_name": "serving",
    #     "display_name": "Serving",
    #     "color": "#123456",
    #     "values": sorted(SERVING_OPTION),
    #     "doc_type": "dish",
    #     "boost": 2,
    #     "type": "term"
    # }


def get_cart_category(brand_id=None):

    cart_categories = Restaurant.objects.raw("Select d.id, d.cart_category FROM restaurant_dish as d WHERE "
                                              "d.brand_id = %s AND d.cart_category <> 'na'"
                                              "GROUP BY d.cart_category;" % brand_id)

    cart_filter = {}

    for category in cart_categories:
        cart_filter[category.cart_category] =  display_name(category.cart_category)


    return cart_filter


def get_cuisine_filters(brand_id=None):

    cuisines = Restaurant.objects.raw("SELECT a.id, a.name " \
                                          "FROM restaurant_dish as d " \
                                          "LEFT JOIN restaurant_adminmenucategory as a on a.id = d.dish_category_id " \
                                          "WHERE d.brand_id = %s " \
                                          "GROUP BY dish_category_id ;" % brand_id)
    print cuisines
    cuisine_filter_values = {}
    for cuisine in cuisines:
        cuisine_filter_values[cuisine.name] = cuisine.name

    return cuisine_filter_values


def get_food_options_filters(brand_id=None):

    food_options = Restaurant.objects.raw("SELECT d.id, d.food_type " \
                                              "FROM restaurant_dish as d " \
                                              "WHERE d.brand_id = %s " \
                                              "GROUP BY d.food_type ;" % brand_id)
    food_options_values = []
    for food_option in food_options:
        options = food_option.food_type.split(',')
        food_options_values.extend(options)

    food_options_values = set(food_options_values)

    food_options_dict = {}

    for food_options_value in food_options_values:
        food_options_dict[food_options_value] = display_name(food_options_value)

    return food_options_dict


def get_ingredients_filters(brand_id=None):

    dish_ingredients = Restaurant.objects.raw("Select d.id, d.ingredients FROM restaurant_dish as d WHERE "
                                              "d.brand_id = %s AND d.ingredients <> 'na'"
                                              "GROUP BY d.ingredients ORDER BY d.ingredients;" % brand_id)

    ingredients_options = []
    for dish in dish_ingredients:
        ingredients_options.extend(dish.ingredients.split(','))

    ingredients_options = set(ingredients_options)
    ingredients_filter = {}

    for ingredient in ingredients_options:
        ingredients_filter[transform_name(ingredient)] = ingredient.capitalize()

    return ingredients_filter


def get_gravy_type_filters(brand_id=None):

    gravy_types = Restaurant.objects.raw("Select d.id, d.gravy_type FROM restaurant_dish as d WHERE "
                                              "d.brand_id = %s AND d.gravy_type <> 'na'"
                                              "GROUP BY d.gravy_type ORDER BY d.gravy_type;" % brand_id)

    gravy_types_filters = {}
    for gravy_type in gravy_types:
        gravy_types_filters[gravy_type.gravy_type] = display_name(gravy_type.gravy_type)

    return gravy_types_filters


def get_bone_boneless_filters(brand_id=None):

    bone_boneless = Restaurant.objects.raw("Select d.id, d.has_bone FROM restaurant_dish as d WHERE "
                                              "d.brand_id = %s AND d.has_bone <> 'na'"
                                              "GROUP BY d.has_bone ORDER BY d.has_bone;" % brand_id)

    bone_boneless_filters = {}
    for has_bone in bone_boneless:
        bone_boneless_filters[has_bone.has_bone] = display_name(has_bone.has_bone)

    return bone_boneless_filters


def get_spicy_type_filters(brand_id=None):

    spicy_types = Restaurant.objects.raw("Select d.id, d.spicy_type FROM restaurant_dish as d WHERE "
                                              "d.brand_id = %s GROUP BY d.spicy_type ORDER BY d.spicy_type;" % brand_id)

    spicy_types_filters = {}
    for spicy_type in spicy_types:
        spicy_types_filters[spicy_type.spicy_type] = display_name(spicy_type.spicy_type)

    return spicy_types_filters

menu_filters = {
    "cart_category": {
        "display_name": "Sections",
        "method": get_cart_category
    },
    "food_type": {
        "display_name": "Veg/Non-Veg",
        "method": get_food_options_filters
    },
    "ingredients": {
        "display_name": "Ingredients",
        "method": get_ingredients_filters
    },
    "dish_category": {
        "display_name": "Cuisines",
        "method": get_cuisine_filters
    },
    "spicy_type": {
        "display_name": "Spicy",
        "method": get_spicy_type_filters
    },
    "has_bone": {
        "display_name": "Has Bone",
        "method": get_bone_boneless_filters
    },
    "gravy_type": {
        "display_name": "Gravy",
        "method": get_gravy_type_filters
    },
}


def get_restaurant_filters():
    # Only filters for restaurant are returned
    for filter in restaurant_filters:
        a = filter["values"]
        filter["values"] = OrderedDict(a)

        if "na" in filter["values"]:
            filter["values"].pop("na")

    return restaurant_filters


def get_menu_filters(restaurant_id=None):

    if restaurant_id:
        dish_filters = []
        try:
            menu_filters = MenuFilters.objects.filter(restaurant_id=restaurant_id)
            for menu_filter in menu_filters:
                dish_filters.append(
                    {
                        "filter_name":  menu_filter.filter_name,
                        "display_name": menu_filter.display_name,
                        "color":        menu_filter.color,
                        "values":       json.loads(menu_filter.values),
                        "doc_type":     "dish",
                        "type":         menu_filter.filter_type
                    }
                )

        except IndexError:
            pass

        return dish_filters


def transform_name(name):
    return name.replace(" ", "_").lower()


def display_name(name):
    return ' '.join([i.capitalize() for i in name.split('_')])


def create_menu_filters(restaurant_id=None):

    if restaurant_id:

        restaurant = Restaurant.objects.get(pk=restaurant_id)

        for key, value in menu_filters.iteritems():
            values = json.dumps(OrderedDict(menu_filters[key]["method"](brand_id= restaurant.brand_id)))

            if MenuFilters.objects.filter(restaurant=restaurant, filter_name=key).exists():
                MenuFilters.objects.filter(restaurant=restaurant, filter_name=key).delete()

            if values.__len__() < 3:
                continue
            else:
                MenuFilters.objects.create(restaurant= restaurant, filter_name=key, color="#337ab7",
                                           display_name=menu_filters[key]["display_name"], values=values)


