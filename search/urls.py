from django.conf.urls import url
from views import SearchView, FilterView, SearchMenuView, PaoBhajiView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url('search/', SearchView.as_view()),
    url('search_menu/', SearchMenuView.as_view()),
    url('filters/', FilterView.as_view()),
    url('paobhaji/', PaoBhajiView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
