from restaurant.models import AdminMenuCategory, Dish, DishVariation
from serializers import SearchSerializer, DishSearchSerializer
from rest_framework.renderers import JSONRenderer

import json


def restaurant_document(restaurant = None):

    cats = []
    dish_names = []
    secondary_cuisines = []

    restaurant_data = SearchSerializer(instance=restaurant).data
    restaurant_dict = json.loads(JSONRenderer().render(restaurant_data))
    print 'restaurant', restaurant, restaurant.id
    dishes = Dish.objects.filter(brand= restaurant.brand)
    print 'dish count', dishes.count()
    for d in dishes:
        dish_names.append(d.dish_name)
        variants = DishVariation.objects.filter(dish=d)
        print 'variant_count', variants.count()
        for variant in variants:
            if variant:
                print 'variety', variant.variety
                dish_names.append(variant.variety + " " + d.dish_name)
            else:
                dish_names.append(d.dish_name)

    try:

        secondary_cuisines = [a.name for a in AdminMenuCategory.objects.filter(id__in = str(restaurant.secondary_cuisine).split(","))]
    except Exception as e:
        print restaurant.secondary_cuisine
        secondary_cuisines = []

    r_json={
        "brand_name": restaurant.brand.name,
        "cuisine_categories": cats,
        "dish_names": dish_names,
        "is_chain": restaurant.brand.is_chain,
        "is_healthy": restaurant.brand.is_healthy,
        "food_type": '' if restaurant.brand.food_type is None else restaurant.brand.food_type.split(","),
        "ingredients": '' if restaurant.ingredients is None else restaurant.ingredients.split(","),
        "famous_for": '' if restaurant.famous_for is None else restaurant.famous_for.split(","),
        "secondary_cuisines": secondary_cuisines,
        "locality": restaurant_dict["locality"]["name"],
        "coordinates": "%f,%f" % (restaurant.latitude, restaurant.longitude)

    }
    restaurant_dict.pop("locality", None)
    result = r_json.copy()

    result.update(restaurant_dict)
    return json.dumps(result)


def dish_document(dish=None, restaurant_id=0):

    dish_variations = []
    dish_variation_names = []
    dish_variation_ids = []
    portion_sizes = []
    number_of_pieces = []
    unit_price  = []
    serving_sizes = []
    follower_dishes= []
    follower_dish_ids = []

    try:
        dish_rating = int(dish.restaurant_dishes.get().dish_rating)
    except Exception as e:
        dish_rating = 0
    dish = DishSearchSerializer(instance=dish).data

    dish["dish_category"] = AdminMenuCategory.objects.get(pk= dish["dish_category"]).name
    dish["food_type"] = dish["food_type"].split(",") if dish["food_type"] else []
    dish["ingredients"] = dish["ingredients"].split(",") if dish["ingredients"] else []
    dish["restaurant_id"] = restaurant_id

    for variation in dish["dish_variation_names"]:
        if variation["variety"]:
            dish_variations.append(variation["variety"]+ " " + dish["dish_name"])
            dish_variation_names.append(variation["variety"])
        dish_variation_ids.append(variation["id"])
        portion_sizes.append(variation["portion"])
        number_of_pieces.append(variation["number_of_pieces"])
        unit_price.append(variation["unit_price"])
        serving_sizes.append(variation["serving_size"])

    dish.pop("dish_variation_names", None)

    dish["dish_variations"] = dish_variations
    dish["dish_variation_names"] = dish_variation_names
    dish["dish_variation_ids"] = dish_variation_ids
    dish["portion_sizes"] = portion_sizes
    dish["number_of_pieces"] = number_of_pieces
    dish["unit_price"] = unit_price
    dish["serving_sizes"] = serving_sizes

    for follower in dish["follower_dishes"]:
        follower = follower["follower_dish"]
        follower_dishes.append(follower["dish_name"])
        follower_dish_ids.append(follower["id"])

    dish["follower_dishes"] = follower_dishes
    dish["follower_dish_ids"] = follower_dish_ids
    dish["dish_rating"]  = dish_rating

    dish.pop("follower_dishes", None)



    return json.dumps(dish)


# def full_doc(restaurant= None):
#     # This won't work, You need to create a serializer for this which just adds the dish field
#     cat = BrandMenuCategory.objects.filter(brand=restaurant.brand)
#     cats = []
#     for category in cat:
#         if not category.admin_category.name in cats:
#             cats.append(category.admin_category.name)
#         if not category.given_category_name in cats:
#             cats.append(category.given_category_name)
#
#     restaurant_data = SearchSerializer(instance=restaurant).data
#     restaurant_dict = json.loads(JSONRenderer().render(restaurant_data))
#
#     dish_variations = []
#     dish_variation_names = []
#     dish_variation_ids = []
#     portion_sizes = []
#     number_of_pieces = []
#     unit_price  = []
#     serving_sizes = []
#
#
#     for dish in restaurant_dict["restaurant_dishes"]:
#         #id, dish_name, standard_dish_name
#         dish_name = dish["dish_name"]
#         follower_dishes= []
#         follower_dish_ids = []
#
#         for variation in dish["dish_variation_names"]:
#             if variation["variety"]:
#                 dish_variations.append(variation["variety"]+ " " + dish_name)
#                 dish_variation_names.append(variation["variety"])
#             dish_variation_ids.append(variation["id"])
#             portion_sizes.append(variation["portion"])
#             number_of_pieces.append(variation["number_of_pieces"])
#             unit_price.append(variation["unit_price"])
#             serving_sizes.append(variation["serving_size"])
#
#         dish.pop("dish_variation_names", None)
#
#         dish["dish_variations"] = dish_variations
#         dish["dish_variation_names"] = dish_variation_names
#         dish["dish_variation_ids"] = dish_variation_ids
#         dish["portion_sizes"] = portion_sizes
#         dish["number_of_pieces"] = number_of_pieces
#         dish["unit_price"] = unit_price
#         dish["serving_sizes"] = serving_sizes
#
#         for follower in dish["follower_dishes"]:
#             follower = follower["follower_dish"]
#             follower_dishes.append(follower["dish_name"])
#             follower_dish_ids.append(follower["id"])
#
#         dish["follower_dishes"] = follower_dishes
#         dish["follower_dish_ids"] = follower_dish_ids
#
#
#     r_json={
#         "brand_name": restaurant.brand.name,
#         "cuisine_categories": cats,
#         "is_chain": restaurant.brand.is_chain,
#         "is_healthy": restaurant.brand.is_healthy,
#         "food_type": restaurant.brand.food_type,
#         "locality": restaurant_dict["locality"]["name"],
#
#         "coordinates": "%f,%f" % (restaurant.latitude, restaurant.longitude)
#
#     }
#     restaurant_dict.pop("locality", None)
#     result = r_json.copy()
#
#     result.update(restaurant_dict)
#     return json.dumps(result)

