from django.db import models
from restaurant.models import Restaurant


class MenuFilters(models.Model):

    restaurant = models.ForeignKey(Restaurant)
    filter_name = models.CharField(max_length=255, null=False)
    display_name = models.CharField(max_length=255, null=False)
    color = models.CharField(max_length=25, default="#1E1E1E")
    values = models.CharField(max_length=1055, null=False)
    filter_type = models.CharField(max_length=50, default="term")

    def __str__(self):
        return self.display_name
