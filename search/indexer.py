'''
Manages indexes for Elastic Search
'''

from nomnom.settings.common import ELASTIC_SEARCH
from django.core.exceptions import DisallowedHost
from restaurant.models import Restaurant, Dish
from document_manager import restaurant_document, dish_document

import filters
import mapping
import json, requests


class IndexManager(object):

    BASE_URL = ELASTIC_SEARCH["HOST"]

    def __init__(self, index_name='nomnom', type_name='restaurant', shards=5, replicas=1):
        self.index_name = index_name
        self.shards = shards
        self.replicas = replicas
        self.type_name = type_name
        if not self._index_exists(self.index_name):
            self.create_index(self.index_name)
            self.create_index_mapping()

    def _index_exists(self, index_name):
        url = IndexManager.BASE_URL + index_name

        try:
            response = requests.head(url)
            if response.status_code == 200:
                return True
            else:
                return False
        except Exception:
            print 'ES not available'
            return False

    def create_index(self, index_name):
        request_data = {
                         "settings" : {
                                  "number_of_shards" : self.shards,
                                  "number_of_replicas" : self.replicas
                                  }
                            }
        url = IndexManager.BASE_URL + index_name
        try:
            response = requests.put(url, data=json.dumps(request_data))
            print response.text
            return True
        except DisallowedHost:
            return False

    def create_index_mapping(self):

        url = ELASTIC_SEARCH["HOST"] + self.index_name + '/_mapping/' + self.type_name
        payload = mapping.maps[self.type_name]

        if self._index_exists(self.index_name):
            response = requests.put(url, data=json.dumps(payload))
            print response.text

    def index_all_restaurants(self):
        restaurant = Restaurant.objects.filter(is_deleted=False)

        for r in restaurant:
            document = restaurant_document(r)
            IndexManager.index_doc(document=document, index_name=self.index_name, type=self.type_name)
            self.index_all_dishes(r)

    def index_all_dishes(self, restaurant):
        dishes = Dish.objects.filter(brand=restaurant.brand)

        for dish in dishes:
            document = dish_document(dish, restaurant.id)
            IndexManager.index_doc(document=document, index_name=self.index_name, type="dish")
        filters.create_menu_filters(restaurant.id)

    @staticmethod
    def index_doc(document, index_name, type):

        url = IndexManager.BASE_URL + index_name + '/' + type

        response = requests.post(url, document)
        print response.text


    @staticmethod
    def delete_index(index_name):

        URL = ELASTIC_SEARCH["HOST"] + index_name
        try:
            response = requests.delete(URL)
            print response
            return True
        except DisallowedHost:
            print 'ES not available'
            return False
