maps = {
"restaurant" :  {
    "restaurant": {

        "properties": {
            "id": {"type": "integer", "index_name": "restaurant_id", "index": "analyzed"},
            "brand_name": {"type": "string", "index": "analyzed",
                           "analyzer": "standard"},
            "primary_cuisine": {
                "type": "string", "index": "analyzed",
                "analyzer": "standard"
            },
            "secondary_cuisines": {
                "type": "string", "index": "analyzed",
                "analyzer": "standard"
            },

            "is_chain": {"type": "boolean"},
            "is_healthy": {"type": "boolean"},
            "food_type": {"type": "string", "index": "analyzed", "analyzer": "standard"},

            "address": {"type": "string", "index": "not_analyzed"},
            "locality": {"type": "string", "index": "analyzed", "analyzer": "standard"},
            "coordinates": {"type": "geo_point", "geohash": True},

            "cost_for_two": {"type": "integer", "index": "analyzed", "analyzer": "standard"},
            "minimum_delivery_order": {"type": "integer", "index": "analyzed", "analyzer": "standard"},
            "estimated_preparation_time": {"type": "integer", "index": "analyzed", "analyzer": "standard"},
            "estimated_delivery_time": {"type": "integer", "index": "analyzed", "analyzer": "standard"},

            "opening_time": {"type": "integer"},
            "closing_time": {"type": "integer"},


            "famous_for": {"type": "string", "index": "analyzed", "analyzer": "standard"},
            "ingredients": {"type": "string", "index": "analyzed", "analyzer": "standard"},
            "nomnom_rating": {"type": "float", "index": "analyzed", "analyzer": "standard"},
            "zomato_rating": {"type": "float", "index": "analyzed", "analyzer": "standard"},
            "nomnom_review": {"type": "string", "index": "not_analyzed"},

            "is_b2c_partner": {"type": "boolean"},
            "is_b2b_partner": {"type": "boolean"},
            "has_special_delivery": {"type": "boolean"},
            "is_b2b_call_partner": {"type": "boolean"},
            "is_b2b_delivery_partner": {"type": "boolean"},

            "is_featured": {"type": "boolean"},

            "vat_tax_rate": {"type": "float"},
            "service_tax_rate": {"type": "float"},

            "nomnom_percentage": {"type": "float", "index": "analyzed", "analyzer": "standard"},

            "description": {"type": "string"},

            "dish_names": {"type": "string", "index": "analyzed", "analyzer": "standard"}




}
          }

},

"dish": {

      "dish": {
                "type": "nested",
                "properties": {
                    "id": {"type": "string"},

                    "dish_name": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "standard_dish_name": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "dish_variations": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "dish_variation_names": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "dish_variation_ids": {"type": "integer"},

                    "cart_category": {"type": "string"},
                    "dish_category": {"type": "string", "index": "analyzed", "analyzed": "standard"},

                    "portion_sizes": {"type": "string"},
                    "number_of_pieces": {"type": "string"},
                    "unit_price": {"type": "integer"},
                    'serving_sizes': {"type": "string"},


                    "follower_dishes": {"type": "string"},
                    "follower_dish_ids": {"type": "string"},

                    "food_type": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "ingredients": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "oil_content": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "has_bone": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "gravy_type": {"type": "string"}, #"index": "analyzed", "analyzer": "standard"},
                    "spicy_type": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "serving": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "initial_cooking": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "final_cooking": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "frying_type": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "taste": {"type": "string", "index": "analyzed", "analyzer": "standard"},
                    "specials": {"type": "string", "index": "analyzed", "analyzer": "standard"},

                    "dish_rating": {"type": "integer"},

                    "description": {"type": "string"},
                    "details": {"type": "string"},
                    "other_comments": {"type": "string"},

                  }
            }

}
}