import json
import traceback

from query_manager import QueryManager
from rest_framework.views import APIView
from filters import get_restaurant_filters, get_menu_filters
from nomnom.response import JSONResponse
from nomnom.choices import *
from utilities.raygun import beam



class SearchView(APIView):

    def post(self, request):
        """
        Summary: Returns the search according to parameters

        ---

        parameters:
            - name: doc_type
              required: false
              type: string
            - name: query_terms
              required: false
              type: json Array
            - name: filters
              required: false
              type: json Array
            - name: boosters
              required: false
              type: json Array
            - name: latlong
              type: json Array
              required: false
            - name: offset
              type: integer
              required: false
            - name: count
              type: integer
              required: false
        """

        try:
            doc_type = request.data.get("doc_type", 'restaurant')
            query_terms = request.data.get("query_terms",[])
            filters = request.data.get("filters",[])
            boosters = request.data.get("boosters",[])
            latlong = request.data.get("latlong",[28.3169,77.2090])
            offset = request.data.get("offset", 0)
            count = request.data.get("count", 0)

            search = QueryManager(index_name='nomnom', doc_type=doc_type, query_terms=query_terms, filters=filters,
                                  latlong= latlong, boosters=boosters)

            result = json.loads(search.get_results(offset=offset, count=count))
            if 'hits' in result.get("hits", {}):

                result_dict = result['hits']['hits']
                result_dict = json.dumps(result_dict)

                return JSONResponse(data=result_dict, search=True, status=200)
            else:
                return JSONResponse(result, status=400)
        except Exception as e:
            beam(e)
            print traceback.format_exc(e)
            return JSONResponse({"Error": "Missing parameters in request"}, status=400)


class SearchMenuView(APIView):

    def post(self, request):

        try:
            filters = request.data.get("filters",[])


            search = QueryManager(index_name='nomnom', doc_type='dish', query_terms=[], filters=filters,
                                  latlong= [], boosters=[])

            result = json.loads(search.get_results(offset=0, count=1000))

            map_dict = {}
            result_dict = []
            variants =[]
            i = 0

            for c in CART_CATEGORY:
                result_dict.append({
                    "cart_category_name": c[0],
                    "display_name": c[1],
                    "items":[]
                })
                map_dict[c[0]] = i
                i += 1

            for r in result['hits']['hits']:
                i= 0
                r = r['_source']
                r["variants"] = []
                j = 0
                variant_position = {}


                # collect variations of a dish
                if len(r["dish_variation_names"]):
                    for variant in r["dish_variation_names"]:

                        if not variant in variant_position:
                            r["variants"].append({"name": variant, "line_item": []})
                            variant_position[variant] = j
                            j += 1

                        r["variants"][variant_position[variant]]["line_item"].append({"variation_id": r["dish_variation_ids"][i],
                                 "portion_size": r["portion_sizes"][i],
                                 "quantity": 0,
                                 "unit_price": r["unit_price"][i]})
                        i += 1
                else:
                    r["dish_name"]  = "Pricing missing for this dish"

                for e in ["dish_variation_names", "dish_variations", "dish_variation_ids",
                          "portion_sizes", "number_of_pieces", "unit_price", "serving_sizes"]:
                    r.pop(e, None)

                # Put a dish in a cart category dict

                result_dict[map_dict[r['cart_category']]]['items'].append(r)

            #print result_dict
            response= []
            for r in result_dict:
                if len(r["items"]) > 0:
                    response.append(r)

            return JSONResponse(data=json.dumps(response), search=True, status=200)
        except Exception as e:
            beam(e)
            print traceback.format_exc(e)
            return JSONResponse({"Error": traceback.format_exc(e)}, status=400)


class FilterView(APIView):
    # Endpoint: /filters
    def get(self, request):
        """
        summary: Get a list of search filters

        ---

        """
        try:
            if request.query_params["type"] == 'restaurant':
                filters = get_restaurant_filters()
            else:
                filters = get_menu_filters(restaurant_id=request.query_params["restaurant_id"])
        except Exception as e:
            beam(e)
            filters = []

        return JSONResponse(filters, status=200)


class PaoBhajiView(APIView):

    def get(self, request):
        from search.indexer import IndexManager

        # IndexManager.delete_index('nomnom')
        indexer = IndexManager()
        indexer.index_all_restaurants()

        return JSONResponse({"Success": True}, status=200)



