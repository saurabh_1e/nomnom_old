from nomnom.settings.common import ELASTIC_SEARCH
from utilities.raygun import beam

import json, requests


class QueryManager(object):

    BASE_URL = ELASTIC_SEARCH["HOST"]
    FUNCTION_MAP = {
        "coordinates": {
            "type": "gauss",
            "origin": {"lat": 28.3169, "lon": 77.2090},
            "offset": "1.5km",
            "scale": "200000km",
            "weight": "3"
        },
        "zomato_rating": {
            "type": "gauss",
            "origin": "4.7",
            "offset": "0.3",
            "scale": "0.8",
            "weight": "1.5"
        }

    }


    restaurant_multi_match = ["primary_cuisine", "brand_name","dish_names"]
    dish_multi_match = ["dish_name", "dish_variations", "dish_variation_names"]

    def __init__(self, index_name, doc_type,  query_terms=[], filters=[], boosters=[], latlong=(28.3169,77.2090)):

        self.index_name = index_name
        self.doc_type = doc_type
        self.query_terms = query_terms
        self.filters = filters
        self.boosters = boosters
        if self.doc_type == 'restaurant':
            # self.functions = []
            self.functions = QueryManager.FUNCTION_MAP
            self.functions["coordinates"]["origin"]["lat"] = latlong[0]
            self.functions["coordinates"]["origin"]["lon"] = latlong[1]
        else:
            self.functions = []
        self.query_json = self.get_main_query()

    def get_results(self, offset=0, count=10):

        url = self.BASE_URL + self.index_name + "/" + self.doc_type + "/_search/"
        try:
            response = requests.get(url, data=json.dumps(self.query_json))
            # print 'Result text: \n\n\n\n'
            # print response.text
            return response.text
        except Exception as e:
            beam(e)
            return False

   # This is pre set according to use case, you might want to generalize this later
    def get_main_query(self):
        main_query = {
            "from" : 0, "size" : 1000,
            "query": {"function_score": {"functions": [], "query": {}, "score_mode": "sum", "boost_mode": "sum"
                                                  }
                               }
                      } #

        for key in self.functions:
            main_query["query"]["function_score"]["functions"].append(
                 self.get_function_query(field= key, function=self.functions[key]))
        if self.doc_type.__eq__('restaurant'):
            main_query["query"]["function_score"]["query"] = self.get_filtered_query()
        else:
            main_query["query"]["function_score"]["query"] = self.get_menu_query()
        #print '\n\n\n Main query follows: \n\n'
        print json.dumps(main_query)
        return main_query

    # Gives the function query which is used because we want to sort
    def get_function_query(self, field, function):

        return {
                 function["type"]: {
                     field : {
                         "origin": function["origin"],
                         "offset": function["offset"],
                         "scale": function["scale"]
                     }
                 },
                 "weight": function["weight"]
                }

    # only for restaurant query
    def get_filtered_query(self):
        # CAUTION DO NOT REMOVE THE EMPTY DICT FROM AND OTHERWISE THE QUERY WILL FAIL
        filtered_query= {"filtered": {
                                      "query": {},
                                      "filter": {
                                          "and": []
                                      }


                                     }
                         }

        if self.query_terms:
            filtered_query["filtered"]["query"] = {"bool": { "must": [], "must_not": []}}

            for query in self.query_terms:
                #print query
                # Check if query was meant for all or a selected field
                if query["field_name"] == "all":
                    sub_query = self.get_bool_query(should_query=[self.get_multi_match_query(query["term"])])
                else:
                    sub_query = self.get_multi_match_query(query_phrase=query["term"])

                # Check if query was positive or negative and attach multi match query to must or must_not accordingly
                #print query["negative"]
                if query["negative"]:
                    filtered_query["filtered"]["query"]["bool"]["must_not"].append(sub_query)
                else:
                    filtered_query["filtered"]["query"]["bool"]["must"].append(sub_query)
        else:
            filtered_query["filtered"]["query"] = { "match_all": {}}

        #print filtered_query

        if self.filters:

            for search_filter in self.filters:
                # Check if it is a range or non-range query
                if search_filter["type"] == "range":
                    filtered_query["filtered"]["filter"]["and"].append(self.get_range_filter_or_query(field_name=search_filter["field_name"],
                                                            values=search_filter["value"]))
                elif search_filter["type"] == "term":
                    filtered_query["filtered"]["filter"]["and"].append(
                        self.get_term_filter_or_query(field_name=search_filter["field_name"],
                                                   term=search_filter["value"]))
        else:
            filtered_query["filtered"]["filter"]["and"].append({})

        #filtered_query["filtered"]["filter"] = inner_query

        # print 'THIS IS FILTERED QUERY'
        # print json.dumps(filtered_query)
        #print filtered_query
        return filtered_query

    def get_menu_query(self):

        # for menu filter by restaurant and boost by everything else


        #filtered_query= {"filtered": {"query": {}, "filter": {}}}
        boosted_query = {"bool": { "must": [] , "should": []}}
        #inner_query = {"and": []}

        # TODO Would we have anything in query terms when pulling up menu?
        # for query in self.query_terms:
        #     print query
        #     # Check if query was meant for all or a selected field
        #     sub_query = self.get_multi_match_query(query_phrase=query["term"], boost=2)
        #
        #     # Check if query was positive or negative and attach multi match query to must or must_not accordingly
        #     print query["negative"]
        #     if query["negative"]:
        #         filtered_query["filtered"]["query"]["bool"]["should_not"].append(sub_query)
        #     else:
        #         filtered_query["filtered"]["query"]["bool"]["should"].append(sub_query)

        for filter in self.filters:
            boosted_query["bool"]["must"].append(self.get_term_filter_query(field_name=filter["field_name"],
                                               term=filter["value"]))

        for booster in self.boosters:
            sub_query = self.get_match_query(booster["value"], booster["field_name"], booster["boost"])
            boosted_query["bool"]["should"].append(sub_query)

        #print filtered_query
        return boosted_query

    def get_match_query(self, query_phrase, field, boost=1):
       match_dict = {
           "match": {
               field: {
                   "query" : query_phrase,
                   "boost": boost
                }
           }}
       return match_dict

    def get_multi_match_query(self, query_phrase, boost=1):
        if self.doc_type == 'restaurant':
            fields = QueryManager.restaurant_multi_match
        else:
            fields = QueryManager.dish_multi_match

        multi_match_dict = {
            "multi_match": {
            "query": "%s" % query_phrase ,
            "fields": fields,
            "boost": boost
            }}
        return multi_match_dict

    def get_bool_query(self, should_query=[], must_query=[], minimum_should_match=0):
        #gets the inner bool query for multi match and nested queries. This would be a should
        bool_query = {"bool": {}}

        if should_query:
            bool_query["bool"]["should"] = []
            for should_match in should_query:
                bool_query["bool"]["should"].append(should_match)
            if minimum_should_match > 0:
                bool_query["bool"]["minimun_should_match"] = minimum_should_match

        if must_query:
            bool_query["bool"]["must"] = []
            for must_match in must_query:
                bool_query["bool"]["must"].append(must_match)

        return bool_query

    def get_range_filter_query(self, field_name, gte=0, lte=False):
        range_filter = {"range": {field_name: {}}}


        range_filter["range"][field_name]["gte"] = gte

        if lte:
            range_filter["range"][field_name]["lte"] = lte

        return range_filter

    def get_range_filter_or_query(self, field_name, values=[{"gte":0,"lte":False}]):

        q = { "or": []}

        for v in values:
            q["or"].append(self.get_range_filter_query(field_name, v["gte"], v["lte"]))

        return q

    def get_term_filter_query(self, field_name, term):

        return {"term": {field_name: term}}

    def get_term_filter_or_query(self, field_name, term):
        q = {"or": []}
        for t in term:
            q["or"].append({ "term": {
                                       field_name: t
                                     }
                                   })
        return q


