from rest_framework import serializers
from restaurant.models import Restaurant, Dish, DishVariation, DishFollower
from crm.models import Locality


class DishVariationSearchSerializer(serializers.ModelSerializer):

    class Meta:
        model = DishVariation
        fields = ('id', 'serving_size', 'number_of_pieces', 'unit_price',
                  'portion', 'variety')


class DishNameSearchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Dish
        fields = ('id', 'dish_name')


class DishFollowerSearchSerializer(serializers.ModelSerializer):
    follower_dish = DishNameSearchSerializer()

    class Meta:
        model = DishFollower
        fields = ('follower_dish', )


class DishSearchSerializer(serializers.ModelSerializer):
    dish_variation_names = DishVariationSearchSerializer(many=True)
    follower_dishes = DishFollowerSearchSerializer(many=True)

    class Meta:
        model = Dish
        fields = ('id',

                  'dish_name', 'standard_dish_name',
                  'dish_variation_names',

                  'cart_category', "dish_category",

                  'follower_dishes',

                  'food_type', 'ingredients', 'oil_content', 'has_bone', 'gravy_type', 'spicy_type',
                  'serving', 'initial_cooking', 'final_cooking', 'frying_type','taste',
                  'specials',

                  'description', 'details', 'other_comments')


class LocalitySearchSerializer(serializers.ModelSerializer):

    class Meta:
        model = Locality
        fields = ('name', )


class SearchSerializer(serializers.ModelSerializer):

    locality = LocalitySearchSerializer()
    primary_cuisine = serializers.SerializerMethodField(source='get_primary_cuisine')


    class Meta:
        model = Restaurant
        exclude = ('city', 'is_deleted',  'lunch_hours', 'dinner_hours',
                   'latitude', 'longitude', 'has_gps_printer',
                   'famous_for', 'ingredients', 'secondary_cuisine', 'has_to_be_called')

    def get_primary_cuisine(self, obj):
        return obj.primary_cuisine.name