from django.db import models
from nomnom.choices import *


class AdminMenuCategory(models.Model):
    '''
    Admin populated categories for a menu
    '''
    name = models.CharField(max_length=125, unique=True)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)


class Brand(models.Model):
    name = models.CharField(max_length=255, null=False)
    company_name = models.CharField(max_length=255)
    is_chain = models.BooleanField(default=False)
    food_type = models.CharField(max_length=255, help_text="Needs Comma separated values such as "
                                                           "pure_veg, non_veg, veg and sea_food")
    is_healthy = models.BooleanField()
    corporate_phone_number = models.CharField(max_length=255, null=True, blank=True)
    corporate_email_ids = models.CharField(max_length=255, null=True, blank=True)
    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.id, self.name)


class Restaurant(models.Model):
    brand = models.ForeignKey('Brand', related_name='restaurant_brand')
    address = models.CharField(max_length=255, null=False)
    locality = models.ForeignKey('crm.Locality', null=True)
    city = models.ForeignKey('crm.City', null=True)
    is_deleted = models.BooleanField(default=False)

    primary_cuisine = models.ForeignKey(AdminMenuCategory, default=1)
    secondary_cuisine = models.CharField(max_length=1025, null=True, default='')

    cost_for_two = models.IntegerField(null=False)
    minimum_delivery_order = models.IntegerField(null=False)
    estimated_preparation_time = models.IntegerField(null=False, help_text="Number only in minutes")
    estimated_delivery_time = models.IntegerField(null=True, blank=True, help_text="Estimated delivery time in minutes, numbers only")
    ingredients = models.CharField(max_length=1055, null=True, blank=True, help_text="Comma separated values of dishes"
                                                                         " with ingredients at this restaurant")
    opening_time = models.IntegerField(default=10, help_text="24 Hour time format")
    closing_time = models.IntegerField(default=22, help_text="24 Hour time format")
    lunch_hours = models.CharField(max_length=10, blank=True)
    dinner_hours = models.CharField(max_length=10, blank=True)

    famous_for = models.CharField(max_length=1055, help_text="Comma separated values of things"
                                                             " this place is famous for", blank=True)
    zomato_rating = models.DecimalField(max_digits=3, decimal_places=1)
    nomnom_rating = models.DecimalField(max_digits=3, decimal_places=1, help_text='Composite rating based on dish ratings, '
                                                             'street intel and zomato rating', blank=True)
    is_featured = models.BooleanField(default=False)

    nomnom_review = models.CharField(max_length=1024, null=True, blank=True)

    latitude = models.DecimalField(decimal_places=6, max_digits=10, default=0.0, null=True, blank=True)
    longitude = models.DecimalField(decimal_places=6, max_digits=10, default=0.0, null=True, blank=True)

    is_b2c_partner = models.BooleanField(default=False)
    is_b2b_partner = models.BooleanField(default=False)
    has_special_delivery = models.BooleanField(default=False)
    is_b2b_call_partner = models.BooleanField(default=False)
    is_b2b_delivery_partner = models.BooleanField(default=False)
    has_gps_printer = models.BooleanField(default=False)
    has_to_be_called = models.BooleanField(default=False)
    has_merchant_app = models.BooleanField(default=False)

    service_tax_rate = models.DecimalField(decimal_places=2, max_digits=4, blank=True)
    vat_tax_rate = models.DecimalField(decimal_places=2, max_digits=4, blank=True)
    nomnom_percentage = models.DecimalField(decimal_places=2, max_digits=4, default=0.0)

    description = models.CharField(max_length=1024, null=True, blank=True)
    # Reverse relationships to dishes and cuisines
    restaurant_dishes = models.ManyToManyField('Dish', through='Menu', through_fields=('restaurant', 'dish'))

    def __str__(self):
        return "%s - %s - %s" % (self.id, self.brand.name, self.address)


class Dish(models.Model):

    brand = models.ForeignKey('Brand')
    dish_name = models.CharField(max_length=125, null=False)
    standard_dish_name = models.CharField(max_length=120, null=False)
    cart_category = models.CharField(max_length=25, null=True, choices=CART_CATEGORY, blank=True)
    dish_category = models.ForeignKey(AdminMenuCategory)

    food_type = models.CharField(max_length=1055, null=False, help_text="comma separated values")
    ingredients = models.CharField(max_length=1055, null=False, help_text="comma separated values")
    oil_content = models.CharField(max_length=25, choices=OIL_CONTENT)
    has_bone = models.CharField(max_length=20, choices=BONE)
    gravy_type = models.CharField(max_length=20, choices=GRAVY)
    spicy_type = models.CharField(choices=SPICY, max_length=25, null=False)
    serving = models.CharField(choices=SERVING_OPTION, max_length=25)
    initial_cooking = models.CharField(choices=INITIAL_COOKING, null=True, max_length=25)
    final_cooking = models.CharField(choices=FINAL_COOKING, null=True, max_length=25)
    frying_type = models.CharField(choices=FRY, max_length=25)
    taste = models.CharField(choices=TASTE, max_length=255)
    specials = models.CharField(choices= SPECIAL, max_length=255)

    description = models.CharField(max_length=1024, null=True, blank=True)
    details = models.CharField(max_length=1024, null=True, blank=True)
    other_comments = models.CharField(max_length=1024, null=True, blank=True)

    is_taxable = models.BooleanField(default=True)

    is_deleted = models.BooleanField(default=False)

    def __str__(self):
        return ("%s - %s") % (self.id, self.dish_name)


class Menu(models.Model):
    # A dish is associated with a restaurant and has a category object
    # Category object specifies the admin cuisine name and the brand given cuisine name
    dish = models.ForeignKey(Dish, related_name='restaurant_dishes')
    restaurant = models.ForeignKey(Restaurant, related_name='restaurant_menu')
    dish_rating = models.DecimalField(decimal_places=2, max_digits=4, blank=True,
                                      help_text="Will vary for every restaurant under a brand")

    class Meta:
        unique_together = (('dish', 'restaurant'),)

    def __str__(self):
        return "Menu for %s" % self.restaurant.brand.name


class DishVariation(models.Model):
    # Shares a one to many relationship with dish
    dish = models.ForeignKey(Dish, null=False, related_name="dish_variation_names")
    serving_size = models.IntegerField(default=1, choices=SERVING_SIZE, help_text="1/2/3/4 people")
    number_of_pieces = models.IntegerField(default=0, help_text="Not applicable in all cases")
    unit_price = models.IntegerField(null=False, default=0, help_text="Would vary with serving size and variety")
    portion = models.CharField(max_length=255, null=False, choices=PORTION,
                                default=PORTION[0][0], help_text="Small/Medium/Large, indicative only." \
                                                                  " Not to be filtered on searched on")
    variety = models.CharField(max_length=25, choices=VARIETY, null=True, blank=True, help_text="Can be empty for the"
                                                                             "dishes with no variations")
    customizations = models.CharField(max_length=2000, null=True, blank=True, help_text="Customizations for places like Subway")
    description = models.CharField(max_length=255, null=True, blank=True, help_text='Restaurant + Variety specific description')

    def __str__(self):
        return ("%s - %s") % (self.id, self.dish.dish_name)


class DishFollower(models.Model):
    # A dish can typically have an accompanying dish, such as French Fries with a Burger
    dish = models.ForeignKey(Dish, null=False, related_name='follower_dishes')
    follower_dish = models.ForeignKey(Dish, null=False, related_name='follower_dish')

    def __str__(self):
        return "%s followed by %s" % (self.dish.dish_name, self.follower_dish.dish_name)


class PhoneNumber(models.Model):
    # Phone numbers of a restaurant
    number = models.CharField(max_length= 20, null=False)
    restaurant = models.ForeignKey(Restaurant, null=False, related_name='numbers')
    number_type = models.CharField(max_length=25, choices=NUMBER_TYPE)

    def __str__(self):
        return self.number


class EmailId(models.Model):
    # Email ids of a restaurant
    email = models.EmailField(null=False)
    restaurant = models.ForeignKey(Restaurant, null=False)


class CouponCode(models.Model):
    name = models.CharField(max_length=20)
    restaurant = models.ForeignKey(Restaurant, null=False)
