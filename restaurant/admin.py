from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from restaurant.models import *


class RestaurantInline(admin.TabularInline):
    model = Restaurant


class DishInline(admin.TabularInline):
    model = Dish


class BrandInline(admin.TabularInline):
    model = Brand


class DishResource(resources.ModelResource):
    class Meta:
        model = Dish


class DishAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = DishResource


class RestaurantResource(resources.ModelResource):
    class Meta:
        model = Restaurant
        exclude = ('restaurant_dishes', )


class AdminMenuCategoryResource(resources.ModelResource):
    class Meta:
        model = AdminMenuCategory


class RestaurantAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = RestaurantResource


class AdminMenuCategoryAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = AdminMenuCategoryResource


class BrandResource(resources.ModelResource):
    class Meta:
        model = Brand


class BrandAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = BrandResource



class MenuResource(resources.ModelResource):
    class Meta:
        model = Menu


class MenuAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = MenuResource


class DishFollowerResource(resources.ModelResource):

    class Meta:
        model = DishFollower


class DishFollowerAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = DishFollowerResource


class DishPricingResource(resources.ModelResource):
    class Meta:
        model = DishVariation


class DishPricingAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = DishPricingResource



class PhoneNumberResource(resources.ModelResource):
    class Meta:
        model = PhoneNumber


class PhoneNumberAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resources_class = PhoneNumberResource


class EmailIdResources(resources.ModelResource):
    class Meta:
        model = EmailId


class EmailIdAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    resource_class = EmailIdResources

admin.site.register(Brand, BrandAdmin)
admin.site.register(Restaurant, RestaurantAdmin)
admin.site.register(AdminMenuCategory, AdminMenuCategoryAdmin)
admin.site.register(Dish, DishAdmin)
admin.site.register(Menu, MenuAdmin)
admin.site.register(DishVariation, DishPricingAdmin)
admin.site.register(DishFollower, DishFollowerAdmin)
admin.site.register(PhoneNumber, PhoneNumberAdmin)
admin.site.register(EmailId, EmailIdAdmin)
