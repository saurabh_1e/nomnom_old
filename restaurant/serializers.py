from rest_framework import serializers
from models import Restaurant, Dish, DishVariation


class RestaurantOrderSerializer(serializers.ModelSerializer):
    brand_name = serializers.SerializerMethodField(source='get_brand_name')
    numbers = serializers.StringRelatedField(many=True)
    city = serializers.SerializerMethodField(source='get_city')

    class Meta:
        model = Restaurant
        fields = ('brand_name', 'address', 'city', 'id', 'numbers', 'latitude', 'longitude', 'vat_tax_rate', 'service_tax_rate')

    def get_brand_name(self, obj):
        return obj.brand.name

    def get_city(self, obj):
        return obj.city.name


class DishSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dish
        fields = ('dish_name', 'id')


class DishLineItemSerializer(serializers.ModelSerializer):

    class Meta:
        model = Dish
        fields = ('dish_name', 'standard_dish_name')


class DeliveryDishVariationSerializer(serializers.ModelSerializer):

    dish = DishLineItemSerializer()


    class Meta:
        model = DishVariation
        fields = ('id', 'dish', 'variety', 'portion')


class ClientSerializer(serializers.ModelSerializer):
    brand_name = serializers.SerializerMethodField(source='get_brand_name')

    class Meta:
        model = Restaurant
        fields = ('id', 'brand_name', 'address', 'city', 'opening_time', 'closing_time', 'service_tax_rate', 'vat_tax_rate')


    def get_brand_name(self, obj):
        return obj.brand.name