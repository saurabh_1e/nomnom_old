from rest_framework.views import APIView
from models import Restaurant
from crm.models import Order, DeliveryBoy, Ticket, OrderLog
from serializers import RestaurantOrderSerializer
from utilities.Logger import Logger
from django.core.exceptions import ObjectDoesNotExist
from django.http.response import JsonResponse
from nomnom.choices import *
from utilities.raygun import beam

import datetime


class RestaurantView(APIView):

    def get(self, request):
        """

        summary: Returns a list of all restaurants

        ---

        serializer: RestaurantOrderSerializer

        """
        restaurants = Restaurant.objects.all()
        drawer = RestaurantOrderSerializer(instance=restaurants, many=True)
        return JsonResponse(drawer.data, status=201)


class RestaurantNotify(APIView):

    def post(self, request):

        """
        summary: Operator confirms order from customer and places order on the crm

        description: Restaurant needs to be notified of the order and we need a confirmation of the same.
                     \n\n Choose closest available delivery boy and assign order
                      \n  Send gcm notification to delivery boy
                      \n Possible failure points are not able to find a delivery boy and push a notification
                      to delivery boy
        ---

        parameters:
            - name: order_id
              type: integer
              required: true

        """

        try:
            order_id = request.data['order_id']
            #radius = request.data["radius"] if 'radius' in request.data else 100
            order = Order.objects.get(id=order_id)
            order.restaurant_notified = True
            order.status = 'confirmed'
            order.save()

            # try:
            #     delivery_boy = DeliveryBoy.objects.get(pk=request.data["delivery_boy_id"])
            # except Exception as e:
            #     delivery_boy = DeliveryBoy.get_available_delivery_boy(order=order, radius = radius)
            #
            # if delivery_boy:
            #     order.delivery_boy = delivery_boy
            #     order.delivery_boy.is_available= False
            #     order.status = 'assigned'
            #     order.save()
            #     order.delivery_boy.save()
            #     try:
            #         delivery_boy.notify({"order_id":order.id, "status": "new"})
            #         OrderLog.objects.create(order=order, message=dict(STOCK_MESSAGES)['delivery_boy_notified'],
            #                                 owner_type='system', owner_id=order.agent.id)
            #         Ticket.objects.filter(id=request.data["ticket_id"]).update(resolved_at = datetime.datetime.now())
            #         return JsonResponse({"Success": 'Delivery boy Notified'}, status=200)
            #     except Exception as e:
            #         beam(e)
            #         Logger.log_ticket(order=order, owner= order.agent, message= dict(STOCK_MESSAGES)['failed_to_notify_delivery_boy'],
            #             owner_type='system', ticket_type='failed_to_notify_delivery_boy')
            #         return JsonResponse({"Success": 'Delivery boy assigned but not notified'}, status=200)
            # else:

            OrderLog.objects.create(order=order, message=dict(STOCK_MESSAGES)['confirmed'],
                                             owner_type='system', owner_id=order.agent.id)
            # Logger.log_ticket(order=order, owner= order.agent, message= dict(STOCK_MESSAGES)['failed_to_assign_delivery_boy'],
            #                           owner_type='system', ticket_type='no_delivery_boy_available')
            return JsonResponse({"Success": 'Order placed, please assign delivery boy.'}, status=200)
        except (KeyError, ObjectDoesNotExist, Exception) as e:
            beam(e)
            return JsonResponse({"Error": "Something went wrong!"})


class WatchDogView(APIView):

    def get(self, request):
        from nomnom.cron_jobs import watchdog

        watchdog()

        return JsonResponse({"success": True}, status=200)


class NadanView(APIView):

    def get(self, request):
        from search.indexer import IndexManager

        IndexManager.delete_index('nomnom')
        indexer = IndexManager()
        indexer.index_all_restaurants()

        return JsonResponse({"success": True}, status=200)


class DeliveryNotificationView(APIView):

    def get(self, request):
        from utilities.sms import SMS
        from utilities.google_gcm import GCM
        from crm.models import CRMAccessToken
        payload = {"order_id": request.query_params["order_id"], "status": "new"}
        tokens = CRMAccessToken.objects.filter(user_id=request.query_params["user_id"])

        tokens = [token.access_token for token in tokens]

        g = GCM()

        g.send_message(tokens, payload)

        s = SMS()
        s.send(mobile_number=request.query_params["contact_number"], sms_text= "Order Id: %s" % request.query_params["order_id"])

        return JsonResponse({"success": True}, status=200)



