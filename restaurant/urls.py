from django.conf.urls import url
from views import RestaurantView, RestaurantNotify, WatchDogView, NadanView, DeliveryNotificationView
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [

    url('restaurant/', RestaurantView.as_view()),
    url('restaurant_notify/', RestaurantNotify.as_view()),
    url('watchdog/', WatchDogView.as_view()),
    url('nadan/', NadanView.as_view()),
    url('dhakka/', DeliveryNotificationView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)